package de.osp.training.bledig.hes_kafka.dlq_consumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DlqConsumerApplication {

	public static void main(String[] args) {
		SpringApplication.run(DlqConsumerApplication.class, args);
	}

}
