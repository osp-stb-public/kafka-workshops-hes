package de.osp.training.bledig.hes_kafka.dlq_consumer;

/**
 * InboundEvent
 * a minimalistic POJO class to represent the inbound event
 */
public class InboundEvent extends KafkaRetryConfig {
    private String eventId;
    private String eventDate;
    private String traceId;
    private double quantity;
    private String productId;

    public InboundEvent() {
    }

    public InboundEvent(String eventId, String eventDate, String traceId, double amount, String productId) {
        this.eventId = eventId;
        this.eventDate = eventDate;
        this.traceId = traceId;
        this.quantity = amount;
        this.productId = productId;
    }

    public String getEventId() {
        return eventId;
    }

    public String getEventDate() {
        return eventDate;
    }

    public String getTraceId() {
        return traceId;
    }

    public double getQuantity() {
        return quantity;
    }

    public String getProductId() {
        return productId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public void setEventDate(String eventDate) {
        this.eventDate = eventDate;
    }

    public void setTraceId(String traceId) {
        this.traceId = traceId;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    @Override
    public String toString() {
        return "InboundEvent{" +
                "eventId='" + eventId + '\'' +
                ", eventDate='" + eventDate + '\'' +
                ", quantity=" + quantity +
                ", productId='" + productId + '\'' +
                '}';
    }
}
