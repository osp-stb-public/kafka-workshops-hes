#!/bin/bash
# Produce sample messages to the Kafka topic inbound
# One valid message and one invalid message

set -e

CONFLUENT_ENV="env-z7g70" # PreProd environment
CONFLUENT_CLUSTER="lkc-1jqd03" # kafka cluster "dev"
KAFKA_TOPIC="hes-training.inbound"

script_dir=$(dirname $0)
cd $script_dir

. .env # load environment variables


# produce invalid message to topic 
message=$(cat <<EOF
{
  "eventId": "$(uuidgen)",
  "traceId": "$(uuidgen)",
  "eventDate": "$(date -u +"%Y-%m-%dT%H:%M:%SZ")",
  "productId": "3631245-439a-4c3a-8c3a-af3f3ac52",
  "quantity": -1
}
EOF
)
echo "$message" | jq -rc . | confluent kafka topic produce $KAFKA_TOPIC --cluster $CONFLUENT_CLUSTER --environment $CONFLUENT_ENV \
  --api-key $CLUSTER_API_KEY --api-secret $CLUSTER_API_SECRET

