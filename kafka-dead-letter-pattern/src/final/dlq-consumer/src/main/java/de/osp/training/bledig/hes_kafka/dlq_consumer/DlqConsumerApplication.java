package de.osp.training.bledig.hes_kafka.dlq_consumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.kafka.annotation.EnableKafka;

import io.github.cdimascio.dotenv.Dotenv;

@EnableKafka // activate Kafka integration in SpringBoot
@SpringBootApplication
public class DlqConsumerApplication {

	public static void main(String[] args) {
		// load environment variables from .env file and set them as system properties
		Dotenv.configure().systemProperties().load();
		// start the Spring Boot application
		SpringApplication.run(DlqConsumerApplication.class, args);
	}

}
