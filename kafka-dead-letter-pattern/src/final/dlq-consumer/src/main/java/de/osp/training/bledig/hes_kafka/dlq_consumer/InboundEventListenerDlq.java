package de.osp.training.bledig.hes_kafka.dlq_consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.DltHandler;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.annotation.RetryableTopic;
import org.springframework.kafka.retrytopic.DltStrategy;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.stereotype.Service;

/**
 * Service to handle inbound events from Kafka.
 * This service is configured to use a retryable topic with a dead letter topic.
 * The service will receive events from the inbound topic and will throw an exception if the event is invalid.
 */
@Service
public class InboundEventListenerDlq {
    private final Logger log = LoggerFactory.getLogger(InboundEventListenerDlq.class);

    @RetryableTopic(
            attempts = "1", // 1 retry
            kafkaTemplate = "retryableTopicKafkaTemplate",  // KafkaTemplate bean name (Producer)
            dltStrategy = DltStrategy.FAIL_ON_ERROR, // Fail on error => send to DLQ
            dltTopicSuffix = ".dlq" // DLQ topic suffix, inbound -> inbound.dlq
    )
    @KafkaListener(
            topics = { "hes-training.inbound" },
            containerFactory="containerFactory" // KafkaListenerContainerFactory bean name, see KafkaConsumerConfig
    )
    public void handleInboundEvent(InboundEvent inboundEvent, @Header(KafkaHeaders.RECEIVED_TOPIC) String topic) {
        log.debug("Receive event on main topic={}, payload={}", topic, inboundEvent);
        if(inboundEvent.getQuantity() < 1) {
            log.warn("Bad event {}: Quantity '{}' too low", inboundEvent.getEventId(), inboundEvent.getQuantity());
            throw new RuntimeException("Quantity too low");
        }
        if(inboundEvent.getProductId() == null || inboundEvent.getProductId().length() < 3) {
            log.warn("Bad event {}: Missing or wrong productId '{}'", inboundEvent.getEventId(), inboundEvent.getProductId());
            throw new RuntimeException("Missing or wrong productId");
        }
        // Process event
        // here should be the call to the business logic service
        log.info("Succesfuel processed event {}", inboundEvent);
    }

    @DltHandler
    public void handleDlqInboundEvent(
            InboundEvent inboundEvent,
            @Header(KafkaHeaders.RECEIVED_TOPIC) String topic,
            @Header(KafkaHeaders.EXCEPTION_MESSAGE) String cause
    ){
        log.info("Event on DLQ topic={}, payload={}, cause= {}", topic, inboundEvent, cause);
        // Process invalid event
        // here should be the call to the business logic service to handle incorrect events
    }
}