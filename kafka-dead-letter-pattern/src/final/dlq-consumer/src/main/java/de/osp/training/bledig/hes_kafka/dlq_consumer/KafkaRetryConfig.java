package de.osp.training.bledig.hes_kafka.dlq_consumer;

import java.util.Map;


import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.support.serializer.JsonSerializer;

/**
 * KafkaRetryConfig
 * a configuration class to provide the producer configuration for Kafka
 * used for producing the failed messages in DLQ
 */
@Configuration
public class KafkaRetryConfig {

    @Autowired(required = true)
    private KafkaBaseConfig kafkaBaseConfig;

    /**
     * Producer Factory for InboundEvent
     * a method to provide the producer factory for Kafka
     * @return a ProducerFactory object
     */
    @Bean
    public ProducerFactory<String, InboundEvent> producerFactory() {
        Map<String, Object> config = kafkaBaseConfig.getKafkaBaseConfig();
        // set the producer factory to don't set the type headers to avoid serialization errors based on the type headers class name
        JsonSerializer<InboundEvent> inboundEventJsonSerializer = new JsonSerializer<>();
        inboundEventJsonSerializer.setAddTypeInfo(false);
        return new DefaultKafkaProducerFactory<>(config, new StringSerializer(), inboundEventJsonSerializer);
    }

    /**
     * KafkaTemplate for InboundEvent
     * a method to provide the KafkaTemplate to produce InboundEvent messages
     * @return a KafkaTemplate object
     */
    @Bean
    public KafkaTemplate<String, InboundEvent> retryableTopicKafkaTemplate() {
        return new KafkaTemplate<>(producerFactory());
    }
}