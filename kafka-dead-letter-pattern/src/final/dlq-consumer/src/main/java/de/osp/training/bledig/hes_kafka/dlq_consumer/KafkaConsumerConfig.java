package de.osp.training.bledig.hes_kafka.dlq_consumer;

import java.util.Map;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.support.serializer.JsonDeserializer;

/**
 * KafkaConsumerConfig
 * a configuration class to provide the consumer configuration for Kafka
 */
@Configuration
public class KafkaConsumerConfig {

    // read the Kafka consumer group id from the application.properties file
    @Value("${spring.kafka.consumer.groupid}")
    private String groupId;

    @Autowired(required = true)
    private KafkaBaseConfig kafkaBaseConfig;


    /**
     * Consumer Factory for InboundEvent
     * a method to provide the consumer factory for Kafka
     * @return a ConsumerFactory object
     */
    @Bean
    public ConsumerFactory<String, InboundEvent> consumerFactory() {
        Map<String, Object> config = kafkaBaseConfig.getKafkaBaseConfig();
        // set the consumer group id, its mandatory for Kafka and should be unique on context of this application
        config.put(ConsumerConfig.GROUP_ID_CONFIG, groupId);

        // set the consumer factory to ignore the type headers to avoid deserialization errors based on the type headers class name
        JsonDeserializer<InboundEvent> inboundEventJsonDeserializer = new JsonDeserializer<>(InboundEvent.class);
        inboundEventJsonDeserializer.ignoreTypeHeaders();

        return new DefaultKafkaConsumerFactory<>(config, new StringDeserializer(), inboundEventJsonDeserializer);
    }

    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, InboundEvent> containerFactory() {
        ConcurrentKafkaListenerContainerFactory<String, InboundEvent> factory = new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(consumerFactory());
        return factory;
    }
}
