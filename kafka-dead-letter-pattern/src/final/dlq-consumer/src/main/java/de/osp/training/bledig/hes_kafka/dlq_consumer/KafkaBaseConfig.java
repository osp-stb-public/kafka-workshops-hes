package de.osp.training.bledig.hes_kafka.dlq_consumer;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * KafkaBaseConfig
 * a configuration class to provide the base configuration for Kafka
 * shared by the KafkaConsumerConfig and KafkaRetryConfig classes
 */
@Configuration
public class KafkaBaseConfig {
    // read the Kafka bootstrap servers from the application.properties file
    @Value(value = "${spring.kafka.bootstrap-servers}")
    private String bootstrapServers;

    // read the Kafka API key and secret from environment variables
    @Value("${CLUSTER_API_KEY}")
    private String apiKey;

    @Value("${CLUSTER_API_SECRET}")
    private String apiSecret;

    /**
     * Build Kafka Base Config
     * a method to provide the base configuration for Kafka
     * @return a Map containing the Kafka base configuration
     */
    public Map<String, Object> getKafkaBaseConfig() {
        Map<String, Object> config = new java.util.HashMap<>();
        config.put("bootstrap.servers", bootstrapServers);
        // set the correct properties for authentication on confluent cloud
        config.put("security.protocol", "SASL_SSL");
        config.put("sasl.mechanism", "PLAIN");
        config.put("sasl.jaas.config", "org.apache.kafka.common.security.plain.PlainLoginModule required username=\""+apiKey+"\" password=\""+apiSecret+"\";");
        return config;
    }
}
