require('dotenv').config()
const {Kafka, logLevel} = require('kafkajs')
const uuid = require('uuid');

const topic = process.env.KAFKA_TOPIC
const broker = process.env.KAFKA_BROKER
const client_id = process.env.KAFKA_PRODUCER_CLIENT_ID
const api_key = process.env.KAFKA_API_KEY
const api_secret = process.env.KAFKA_API_SECRET

const kafka = new Kafka({
  clientId: client_id, // Die client ID wird genutzt zur Identifikation des Producers in einem Kafka Cluster.
                       // Jeder Producer, der sich mit dem gleichen client ID verbindet, wird als derselbe Producer betrachtet.
  brokers: [broker], // Die Broker-URLs, die der Producer verwenden soll, um sich mit dem Kafka Cluster zu verbinden.
                     // Bei Confluent ist es immer die URL thes Bootstrap Servers!
  ssl: true, // Aktiviert SSL Verschlüsselung
  sasl: {
    mechanism: 'plain', 
    username: api_key,
    password: api_secret
  },
  //logLevel: logLevel.DEBUG
})



/**
 * Creates a Kafka producer and sends messages to the specified topic.
 * @param {number} blockAmount - The number of message blocks to produce.
 * @param {number} blockSize - The number of messages in each block.
 * @returns {Promise<void>} - A promise that resolves when the producer finishes producing the messages.
 */
async function produce(blockAmount=1, blockSize = 100) {
  console.log(`Start producing of ${blockAmount}*${blockSize} messages ...`)
  console.time("Produce") // Startet einen Timer, um die Zeit zu messen, die benötigt wird, um die Nachrichten zu produzieren
  const producer = kafka.producer() // Erzeugt eine neue producer instance
  await producer.connect() 
  for (let i = 0; i < blockAmount; i++) {
    const messages = build_test_msg_block(i, blockSize)
    console.debug(`Send ${i + 1}. block of messages`)
    // Senden der Nachrichten an das Topic als ein Block(=Array von Nachrichten)
    await producer.send({
      topic: topic,
      messages: messages,
    })
  }

  await producer.disconnect() // Trennt die Verbindung zum Kafka Cluster. Wichtig, um Ressourcen freizugeben.
  console.timeEnd("Produce")  // Stoppt den Timer und gibt die Zeit aus, die benötigt wurde, um die Nachrichten zu produzieren.
  console.log(`Finished producing of ${blockAmount}*${blockSize} messages.`)
}

/**
 * Builds a test message object.
 * @param {number} i - The index of the message.
 * @returns {string} - The JSON string representation of the test message.
 */
function build_test_msg(i) {
  const msg = {
    eventId: uuid.v4(),
    eventTime: new Date().toISOString(),
    data: `Test message ${i}`
  }
  return JSON.stringify(msg)
}

/**
 * Builds a block of test messages.
 * @param {number} blocksize - The number of messages in the block.
 * @returns {Array} - An array of message objects.
 */
function build_test_msg_block(block_no, blocksize) {
  const messages = []
  for (let i = 0; i < blocksize; i++) {
    const msg = {value: build_test_msg(`${block_no+1}-${i}`)}
    messages[i] = msg
  }
  return messages
} 

produce(2)
