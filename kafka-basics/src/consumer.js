require('dotenv').config()
const {Kafka, logLevel} = require('kafkajs')
const topic = process.env.KAFKA_TOPIC
const broker = process.env.KAFKA_BROKER
const client_id = process.env.KAFKA_CONSUMER_CLIENT_ID
const consumer_group_id = process.env.KAFKA_CONSUMER_GROUP_ID
const api_key = process.env.KAFKA_API_KEY
const api_secret = process.env.KAFKA_API_SECRET
let consumer = null  // Variable für die Consumer-Instanz


const kafka = new Kafka({
  clientId: client_id, // Die client ID wird genutzt zur Identifikation des Consumers in einem Kafka Cluster.
                      // Jeder Consumer, der sich mit dem gleichen client ID verbindet, wird als derselbe Consumer betrachtet.
  brokers: [broker],
  ssl: true,
  sasl: {
    mechanism: 'plain', 
    username: api_key,
    password: api_secret
  },
  //logLevel: logLevel.DEBUG
})




async function consume() {
  // Erzeugt eine neue consumer instance
  consumer = kafka.consumer({
    groupId: consumer_group_id // Die consumer group ID wird genutzt, um die Consumer in einer Gruppe zu identifizieren.
  })

  await consumer.connect() // Verbindet den Consumer mit dem Kafka Cluster
  // Abonniert das Topic, um Nachrichten zu empfangen
  await consumer.subscribe({
    topic: topic, 
    fromBeginning: true  // Startet das Lesen der Nachrichten 
                         // von der letzten Offset-Position der Gruppe.
                         // Wenn diese nicht vorhanden ist vom Anfang des Topics.
  })

  // Startet den Consumer und gibt die Nachrichten aus
  await consumer.run({
    eachMessage: async ({topic, partition, message}) => {
      console.log({
        value: message.value.toString(),
        partition: partition,
      })
    },
  })
}

// Add handler to handle "ctrl-c" to disconnect the consumer
process.on('SIGINT', async () => {
  console.log('Disconnecting consumer ...')
  await consumer.disconnect()
  process.exit(0)
})

consume()
