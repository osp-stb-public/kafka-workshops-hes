#!/bin/bash
# This script builds the documentation on GitLab and moves it to the public directory
# to deploy it as GitLab Pages

set -e

sub_project=$1

echo "Building documentation for $sub_project ..."
cd ${CI_PROJECT_DIR}/$sub_project/slides
asciidoctor -r asciidoctor-diagram --destination-dir /slides/html5 *.adoc
[ -d ./images ] && cp -r ./images /slides/html5/
[ -d ./assets ] && cp -r ./assets /slides/html5/
echo "Moving to pages output folder ..."
mv -v /slides/html5 ${CI_PROJECT_DIR}/public/$sub_project
echo "Documentation build complete!"
ls -lR ${CI_PROJECT_DIR}/public/$sub_project

