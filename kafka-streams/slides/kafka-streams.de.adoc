= Kafka Streams: Möglichkeiten, Vorteile und Herausforderungen
:author: Bernd Ledig, Dresden 2024
:customcss: css/osp-slides3.css
:revealjs_theme: white
:revealjs_slideNumber: true
:imagesdir: ./images
:data-uri:
:source-highlighter: highlightjs
:icons: font

[.text-center]
image::bgnd-img-kafka-streams.webp[abstract-arch, 800]

[.columns]
== Abstrakt

[.column]
image::kafka-streams2.webp[]

[.column]

* Kurzer Überblick über Kafka Streams, eine leistungsstarke Technologie zur Echtzeit-Datenverarbeitung. 
* Möglichkeiten, Vorteile 
* Herausforderungen
* Einfaches Implementierungsbeispiel (Coding-Session)

[.with_img]
== Möglichkeiten von Kafka Streams

image::bgnd-img-kafka-streams.webp[background, size=cover]

=== Möglichkeiten von Kafka Streams

**Echtzeit-Datenverarbeitung**:: Kafka Streams ermöglicht die Verarbeitung von Daten in Echtzeit.
  Das bedeutet, dass Daten sofort nach ihrem Eintreffen verarbeitet werden können, 
  was besonders für Anwendungen wichtig ist, die schnell auf eingehende Daten reagieren müssen.

=== Möglichkeiten von Kafka Streams

**Verteilte Verarbeitung**:: Dank der engen Integration mit Apache Kafka kann Kafka Streams Datenströme 
  auf mehrere Instanzen verteilen, 
  was eine skalierbare und fehlertolerante Datenverarbeitung ermöglicht.

=== Möglichkeiten von Kafka Streams

**Stateful Processing**:: Kafka Streams unterstützt stateful processing, 
  d.h. es kann den Zustand zwischen den Nachrichten beibehalten. 
  Dies ist besonders nützlich für Anwendungen, die Aggregationen, Joins oder Windowoperationen benötigen.

=== Möglichkeiten von Kafka Streams

**Integrierte Fault Tolerance**:: Durch die Verwendung von Kafka als Backbone bietet 
  Kafka Streams integrierte Fehlertoleranz. 
  Daten und Zustände können automatisch wiederhergestellt werden, 
  wenn Knoten ausfallen oder neu gestartet werden.

=== Möglichkeiten von Kafka Streams

**Einfache Integration und Entwicklung**:: Kafka Streams ist ein Java-Bibliothek, 
  die sich einfach in bestehende Anwendungen integrieren lässt. 
  Entwickler können mit bekannten Tools und Paradigmen arbeiten, was die Entwicklung beschleunigt.


[.columns]
== Stream Topologien

[.column]
image::stream-topology.webp[]

[.column]
* Stream Topologien bestimmern die grundlegenden Prozesse, wie die Daten verarbeitet werden.
* Sie bestehen aus Quellen (Sources), Zielen (Sinks) und Operationen (Operators), die die Datenverarbeitung definieren.

=== Filter-Operator

Der Filter-Operator ermöglicht es, Nachrichten zu filtern, die bestimmte Kriterien erfüllen.

image::filter-operator.drawio.png[]

NOTE: Damit ist es auch einfach möglich, ein Stream als Router zu verwenden, um Nachrichten an verschiedene Ziele zu senden.



=== Transform-Operator

Der Transform-Operator ermöglicht es, Nachrichten zu transformieren, z.B. durch Hinzufügen, Entfernen oder Ändern von Feldern.

image::transform-operator.drawio.png[]

NOTE: Damit kann jegliches Mapping von Daten durchgeführt werden, um sie in das gewünschte Format zu bringen.


=== Aggregate

Daten der Nachrichten können aggregiert werden, um z.B. Summen, Durchschnitte oder andere Statistiken zu berechnen. +
Hierbei wird der Zustand zwischen den Nachrichten beibehalten, um Aggregationen über mehrere Nachrichten hinweg zu berechnen.

image::aggregate-prozessing.drawio.png[]

NOTE: Das ist der Eintritt in die Welt des `stateful processing`. +
      Diese ist signifikant komplexer und wird hier nur angedeutet.


=== Join

Daten aus verschiedenen Streams können mithilfe von Joins zusammengeführt werden, um z.B. Daten aus verschiedenen Quellen zu kombinieren.

image::join-operator.drawio.png[]


[.with_img]
== Vorteile von Kafka Streams

image::streams-vorteile.webp[background, size=cover]

=== Vorteile von Kafka Streams

**Leichtgewichtig und Integrierbar**:: Es ist eine Bibliothek, die in jede Java-Anwendung 
  eingebettet werden kann, was die Integration vereinfacht.

**Skalierbarkeit**:: Durch die verteilte Architektur von Kafka kann Kafka Streams 
  problemlos horizontal skaliert werden, um große Datenmengen zu verarbeiten.

=== Vorteile von Kafka Streams

**Flexibilität**:: Kafka Streams bietet eine Vielzahl von Operationen wie 
  Filter, Map, Aggregate und Joins, die eine flexible und mächtige Datenverarbeitung ermöglichen.

NOTE: Während des Stream-Processing können nicht nur Messages sondern auch andere Daten herangezogen oder erzeugt werden.
  Z.B. per Rest-API, Datenbanken, Dateien, etc.

=== Vorteile von Kafka Streams

**End-to-End Semantik**:: Kafka Streams garantiert eine einmalige Verarbeitung 
  von Daten (Exactly-once Semantik), was für viele Anwendungsfälle entscheidend ist.

**Community und Ökosystem**:: Als Teil des Apache Kafka-Ökosystems profitiert Kafka Streams 
  von einer großen und aktiven Community, umfangreicher Dokumentation und zahlreichen Erweiterungen.



[.with_img]
== Herausforderungen von Kafka Streams

image::streams-herausforderungen.webp[background, size=cover]

=== Herausforderungen von Kafka Streams

**Komplexität der Anwendung**:: Die Entwicklung von Anwendungen mit Kafka Streams kann komplex sein, insbesondere wenn stateful processing oder anspruchsvolle Datenverarbeitungslogiken erforderlich sind.

**Feinabstimmung und Monitoring**:: Um die optimale Leistung zu gewährleisten, erfordert Kafka Streams eine sorgfältige Abstimmung und kontinuierliches Monitoring. Das Setzen und Überwachen der richtigen Konfigurationsparameter kann herausfordernd sein.

=== Herausforderungen von Kafka Streams

**Initiale Lernkurve**:: Für Entwickler, die neu in der Welt der Datenströme und der Echtzeit-Datenverarbeitung sind, kann Kafka Streams eine gewisse Lernkurve mit sich bringen.

**Abhängigkeit von Kafka**:: Kafka Streams ist stark an die Verfügbarkeit und Performance des zugrunde liegenden Kafka Clusters gebunden. Probleme im Kafka Cluster können sich direkt auf die Streams-Anwendung auswirken.



[.with_img]
== Live-Coding Session

image::coding.webp[background, size=cover]


=== Implementierung eines einfachen Kafka Streams-Beispiels

Als Beispiel implementieren wir eine einfache Router/Filter als Kafka Streams-Anwendung.

Aufgabe:: Nachrichten aus dem Topic "products" filtern nach "Hoch-Preisigen" Produkten und diese in dem separaten Topic `hes-training.products.high_prices` ablegen.




== Fazit

Zusammenfassend lässt sich sagen, dass Kafka Streams eine mächtige Lösung für die Echtzeit-Datenverarbeitung bietet, 
die sich nahtlos in das Kafka-Ökosystem integriert. 

Ich konnte hier nur einen kleinen Einblick in die Möglichkeiten und Vorteile von Kafka Streams geben.

Es bringt viele Vorteile mit sich, erfordert jedoch auch eine sorgfältige Implementierung und Überwachung, 
um die Herausforderungen zu meistern.


=== Links zum Thema

* https://www.confluent.io/resources/ebook/dummies/[eBook: Apache Kafka Transaction Data Streaming for Dummies]
* https://www.codecentric.de/wissens-hub/blog/stream-processing-kafka-streams-spring-boot[Blog-Beitrag: Stream Processing mit Kafka Streams und Spring Boot]
* https://gitlab.com/osp-stb-public/kafka-workshops-hes[Diese Präsentation und der Source-Code]



[%notitle]
=== Vielen Dank!

image::thank-you.webp[]

