package de.osp.training.bledig.hes_kafka.high_price_router;

import io.github.cdimascio.dotenv.Dotenv;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HighPriceRouterApplication {

	public static void main(String[] args) {
		// load environment variables from .env file and set them as system properties
		Dotenv.configure().systemProperties().load();
		// start the Spring Boot application
		SpringApplication.run(HighPriceRouterApplication.class, args);
	}

}
