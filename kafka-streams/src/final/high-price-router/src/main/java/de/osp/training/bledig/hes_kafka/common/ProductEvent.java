package de.osp.training.bledig.hes_kafka.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductEvent {
    // common event header
    private String eventId;
    private String eventDate;
    private String traceId;

    // business specific data
    private String productId;
    private String name;
    private String description;
    private double price;
}
