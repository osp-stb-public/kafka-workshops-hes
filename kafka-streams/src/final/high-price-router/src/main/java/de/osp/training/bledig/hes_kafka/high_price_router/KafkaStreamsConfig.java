package de.osp.training.bledig.hes_kafka.high_price_router;

import de.osp.training.bledig.hes_kafka.common.ProductEvent;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.KStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafkaStreams;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import org.springframework.kafka.support.serializer.JsonSerde;

import java.util.HashMap;
import java.util.Map;


@Configuration
@EnableKafkaStreams
public class KafkaStreamsConfig {

    private static final Logger logger = LoggerFactory.getLogger(KafkaStreamsConfig.class);
    public static final int HIGH_PRICE_LIMIT = 60;
    public static final String PRODUCTS_TOPIC = "hes-training.products";
    public static final String HIGH_PRICES_TOPIC = "hes-training.products.high_prices";

    /**
     * Diese Methode konfiguriert den Serde für die Produkte.
     * @return Serde<ProductEvent>
     */
    private static Serde<ProductEvent> getJsonSerdeProduct() {
        var jsonSerdeProduct = new JsonSerde<>(ProductEvent.class);
        Map<String, Object> serdeProps = new HashMap<>();
        //serdeProps.put("json.value.type", ProductEvent.class);
        serdeProps.put(JsonDeserializer.TRUSTED_PACKAGES, "*");
        jsonSerdeProduct.configure(serdeProps, false);
        return jsonSerdeProduct;
    }

    /**
     * Diese Methode konfiguriert den Kafka Stream, der die Produkte filtert und in ein neues Topic schreibt.
     * @param streamsBuilder
     * @return KStream<String, ProductEvent>
     */
    @Bean
    public KStream<String, ProductEvent> kStream(StreamsBuilder streamsBuilder) {
        // Erstelle einen Stream, der die Produkte aus dem Topic "hes-training.products" liest
        KStream<String, ProductEvent> stream = streamsBuilder.stream(PRODUCTS_TOPIC,
                Consumed.with(Serdes.String(), getJsonSerdeProduct()));

        // Filtere die Produkte, die teurer als 60 sind und schreibe sie in das Topic "hes-training.products.high_prices"
        stream.filter((key, product) -> {
                    if(product == null) {
                        // Hier wurde eine Tombstone-Nachricht empfangen
                        logger.warn("Received null product");
                        return false;
                    }
                    logger.info("Processing product: id={}, price={}", product.getProductId(), product.getPrice());
                    return product.getPrice() > HIGH_PRICE_LIMIT;
                })
                .to(HIGH_PRICES_TOPIC);

        return stream;
    }
}