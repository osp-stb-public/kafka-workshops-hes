package de.osp.training.bledig.hes_kafka.high_price_router;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HighPriceRouterApplication {

	public static void main(String[] args) {
		SpringApplication.run(HighPriceRouterApplication.class, args);
	}

}
