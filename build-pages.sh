#!/bin/bash
# This script builds the documentation on GitLab and moves it to the public directory
# to deploy it as GitLab Pages

set -e

sub_project=$1

echo "Building documentation for $sub_project ..."
cd ${CI_PROJECT_DIR}/$sub_project
/opt/adoc-reveal/bin/ci-build.sh
echo "Moving to pages output folder ..."
mv -v public ${CI_PROJECT_DIR}/public/$sub_project
echo "Documentation build complete!"
