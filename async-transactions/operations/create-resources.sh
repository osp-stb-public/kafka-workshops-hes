#!/bin/bash

set -e

rm -rf k8s.tmp

cp -r k8s k8s.tmp

export $(grep -v '^#' ./.env | xargs)

find ./k8s.tmp -type f | while read -r file; do
  envsubst < "$file" > "${file}.tmp" && mv "${file}.tmp" "$file"
done

kubectl apply -k k8s.tmp

rm -rf k8s.tmp
