#!/bin/bash
# Kopiert die src-Dateien in den Pod "erp" und startet spring-boot:run

set -e

SERVICE_NAME=postgres

script_dir=`dirname "$0"`
# Wechslen in das Verzeichnis des Scriptes
cd ${script_dir}

# Port-Forwarding zu dem Pod
kubectl port-forward service/${SERVICE_NAME} 5432:5432
