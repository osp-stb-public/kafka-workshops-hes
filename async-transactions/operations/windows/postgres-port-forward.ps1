# Sets up port-forwarding to the "postgres" service

# Variables
$SERVICE_NAME = "postgres"

# Port-forwarding to the service
kubectl port-forward service/$SERVICE_NAME 5432:5432
