# Copies the src files to the "erp" pod and starts spring-boot:run

$DEPLOY_NAME = "erp"

$scriptpath = $MyInvocation.MyCommand.Path
$dir = Split-Path $scriptpath

# Get the pod name
$POD_NAME = kubectl get pods -l app=$DEPLOY_NAME -o jsonpath='{.items[0].metadata.name}'

# Copy files to the pod
kubectl cp $dir\..\..\src\start\erp ${POD_NAME}:/opt/

# Start the application
kubectl exec -it $POD_NAME -- /bin/bash -c "cd /opt/ && ./mvnw spring-boot:run"
