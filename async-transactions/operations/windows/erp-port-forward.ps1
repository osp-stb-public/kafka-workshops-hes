# Sets up port-forwarding to the "erp" deployment

# Variables
$DEPLOY_NAME = "erp"

# Change to the script directory
$script_dir = Split-Path -Parent $MyInvocation.MyCommand.Path
Set-Location $script_dir

# Port-forwarding to the deployment
kubectl port-forward deployment/$DEPLOY_NAME 8080:8080
