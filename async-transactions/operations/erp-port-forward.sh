#!/bin/bash
# Kopiert die src-Dateien in den Pod "erp" und startet spring-boot:run

set -e

DEPLOY_NAME=erp

script_dir=`dirname "$0"`
# Wechslen in das Verzeichnis des Scriptes
cd ${script_dir}

# Port-Forwarding zu dem Pod
kubectl port-forward deployment/${DEPLOY_NAME} 8080:8080
