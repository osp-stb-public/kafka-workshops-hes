#!/bin/bash
# Kopiert die src-Dateien in den Pod "depot1" und startet spring-boot:run

set -e

DEPLOY_NAME=depot2

script_dir=`dirname "$0"`
# Wechslen in das Verzeichnis des Scriptes
cd ${script_dir}

# Wechseln in das Verzeichnis der Quellcode-Dateien
cd ../src/start/submodule
# Ermmitteln des Pods
POD_NAME=$(kubectl get pods -l app=$DEPLOY_NAME -o jsonpath='{.items[0].metadata.name}')
# Kopieren der Dateien in den Pod
kubectl cp . ${POD_NAME}:/opt/

# Starten der Anwendung
kubectl exec -it ${POD_NAME} -- /bin/bash -c "cd /opt/ && ./mvnw spring-boot:run"
