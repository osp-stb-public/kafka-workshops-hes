#!/bin/sh
docker stop -t 1 adoc-processor

set -e

# wechslen in das Script-Verzeichnis
cd $(dirname "$0")

mkdir -p slides/html5
rm -rf slides/html5/*
ln -s ../assets slides/html5/assets

echo "Starte den asciidoctor-Container und verarbeite die Slides"
docker run --rm -v ${PWD}/slides:/slides --platform linux/amd64 bledig2/adoc-revealjs-processor asciidoctor -r asciidoctor-diagram --destination-dir /slides/html5 /slides/*.adoc
sleep 5
cd slides/html5
echo "Starte den Chrome-Browser und zeige die Slides"
#google-chrome *.html &
/Applications/Google\ Chrome.app/Contents/MacOS/Google\ Chrome *.html
