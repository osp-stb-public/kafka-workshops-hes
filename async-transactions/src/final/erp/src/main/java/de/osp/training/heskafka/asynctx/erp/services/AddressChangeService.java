package de.osp.training.heskafka.asynctx.erp.services;

import java.util.List;

import de.osp.training.heskafka.asynctx.erp.dto.AddressChangeInput;
import de.osp.training.heskafka.asynctx.erp.model.AddressChange;

public interface AddressChangeService {
    public AddressChange addAddressChange(AddressChangeInput addressChangeInput, Long orderId);
    public List<AddressChange> getAddressChanges(Long orderId);
    public AddressChange getAddressChange(Long orderId, Long addressChangeId);
}
