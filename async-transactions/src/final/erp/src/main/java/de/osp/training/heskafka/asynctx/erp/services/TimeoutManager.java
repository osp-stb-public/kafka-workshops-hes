package de.osp.training.heskafka.asynctx.erp.services;

import java.time.Instant;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import de.osp.training.heskafka.asynctx.erp.events.TimeoutNachAnfrage;
import de.osp.training.heskafka.asynctx.erp.events.TimeoutNachBeauftragung;
import de.osp.training.heskafka.asynctx.erp.events.KafkaEvent;
import de.osp.training.heskafka.asynctx.erp.model.AddressChange;
import de.osp.training.heskafka.asynctx.erp.model.CustomerOrder;
import de.osp.training.heskafka.asynctx.erp.repositories.CustomerOrderRepository;

@Component
public class TimeoutManager {
    @Autowired
    private CustomerOrderRepository orderRepository;

    @Value("${KAFKA_TOPIC}")
    private String kafkaTopic;

    @Autowired
    private KafkaTemplate<String, Object> kafkaTemplate;

    @Scheduled(fixedRate = 1000)
    @Transactional("transactionManager")
    public void checkTimeout() {
        List<CustomerOrder> orders = getOrdersWithOngoingAddressChanges();

        orders.stream().forEach(order -> {
            order.getAddressChanges().stream()
                    .filter(ac -> ac.isOngoing() && ac.getLastStateChange().plusSeconds(10).isBefore(Instant.now()))
                    .forEach(this::handleTimeout);
        });
    }

    private List<CustomerOrder> getOrdersWithOngoingAddressChanges() {
        return orderRepository.findAll().stream().filter(CustomerOrder::hasOngoingAddressChange).toList();
    }

    private void handleTimeout(AddressChange addressChange) {
        CustomerOrder order = addressChange.getOrder();

        KafkaEvent timeoutEvent;
        if (addressChange.getState().equals(AddressChange.State.WARTEN_AUF_ZUSTIMUNG_ALLER_SUBMODULE)) {
            timeoutEvent = new TimeoutNachAnfrage(order.getId(), addressChange.getId());
        } else if (addressChange.getState().equals(AddressChange.State.BEAUFTRAGT)) {
            timeoutEvent = new TimeoutNachBeauftragung(order.getId(), addressChange.getId());
        } else {
            return;
        }

        kafkaTemplate.send(kafkaTopic, order.getId().toString(), timeoutEvent);
    }
}
