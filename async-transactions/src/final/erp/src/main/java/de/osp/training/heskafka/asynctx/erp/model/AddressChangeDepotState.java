package de.osp.training.heskafka.asynctx.erp.model;

import jakarta.persistence.Embeddable;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;

@Embeddable
public class AddressChangeDepotState {
    private Long depotId;
    @Enumerated(EnumType.STRING)
    private State state;

    public Long getDepotId() {
        return depotId;
    }

    public void setDepotId(Long depotId) {
        this.depotId = depotId;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public static enum State {
        WARTEN_AUF_ZUSTIMMUNG,
        ZUGESTIMMT,
        ABGELEHNT,
        ERFOLGT,
        ABGEBROCHEN,
    }

    @Override
    public String toString() {
        return "AddressChangeDepotState [depotId=" + depotId + ", state=" + state + "]";
    }

}
