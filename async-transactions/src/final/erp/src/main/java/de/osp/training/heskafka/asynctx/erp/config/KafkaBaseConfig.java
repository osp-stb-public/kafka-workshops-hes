package de.osp.training.heskafka.asynctx.erp.config;

import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class KafkaBaseConfig {
    @Value("${KAFKA_BOOTSRAP_SERVER}")
    private String bootstrapServer;

    @Value("${KAFKA_API_KEY}")
    private String apiKey;

    @Value("${KAFKA_API_SECRET}")
    private String apiSecret;

    public Map<String, Object> getKafkaBaseConfig() {
        Map<String, Object> config = new java.util.HashMap<>();
        config.put("bootstrap.servers", bootstrapServer);
        config.put("security.protocol", "SASL_SSL");
        config.put("sasl.mechanism", "PLAIN");
        config.put("sasl.jaas.config", "org.apache.kafka.common.security.plain.PlainLoginModule required username=\"" + apiKey + "\" password=\"" + apiSecret + "\";");
        return config;
    }
}
