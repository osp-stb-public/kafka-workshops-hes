package de.osp.training.heskafka.asynctx.erp.eventhandlers;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import de.osp.training.heskafka.asynctx.erp.events.AdressaenderungZugestimmt;
import de.osp.training.heskafka.asynctx.erp.events.AdressaenderungBeauftragt;
import de.osp.training.heskafka.asynctx.erp.model.AddressChange;
import de.osp.training.heskafka.asynctx.erp.model.AddressChangeDepotState;
import de.osp.training.heskafka.asynctx.erp.model.CustomerOrder;
import de.osp.training.heskafka.asynctx.erp.repositories.CustomerOrderRepository;

@Service
public class AdressaenderungZugestimmtHandler {
    private static final Logger logger = LoggerFactory.getLogger(AdressaenderungZugestimmtHandler.class);

    @Value("${KAFKA_TOPIC}")
    private String kafkaTopic;

    @Autowired
    private KafkaTemplate<String, Object> kafkaTemplate;

    @Autowired
    private CustomerOrderRepository orderRepository;

    @Transactional("transactionManager")
    public void handle(AdressaenderungZugestimmt event) {
        Optional<CustomerOrder> orderOptional = orderRepository.findById(event.getOrderId());
        
        if (orderOptional.isEmpty()) {
            logger.warn("Order not found with ID: {}", event.getOrderId());
            return;
        }
        
        CustomerOrder order = orderOptional.get();
        Optional<AddressChange> addressChangeOptional = order.getAddressChange(event.getAddressChangeId());

        if (addressChangeOptional.isEmpty()) {
            logger.warn("Address change not found with ID: {}", event.getAddressChangeId());
            return;
        }

        AddressChange addressChange = addressChangeOptional.get();

        if (!addressChange.getState().equals(AddressChange.State.WARTEN_AUF_ZUSTIMUNG_ALLER_SUBMODULE)) {
            logger.warn("Address change is not in state WARTEN_AUF_ZUSTIMUNG_ALLER_SUBMODULE");
            return;
        }

        Optional<AddressChangeDepotState> optionalDepotState = addressChange.getDepotState(event.getDepotId());

        if (!optionalDepotState.isPresent()) {
            logger.info("Depot state not found for depotId {}", event.getDepotId());
            return;
        }

        AddressChangeDepotState depotState = optionalDepotState.get();

        if (!depotState.getState().equals(AddressChangeDepotState.State.WARTEN_AUF_ZUSTIMMUNG)) {
            logger.info("Depot state is not in WARTEN_AUF_ZUSTIMMUNG for depotId {}", event.getDepotId());
            return;
        }

        depotState.setState(AddressChangeDepotState.State.ZUGESTIMMT);

        if (addressChange.hasAllDepotsApproved()) {
            addressChange.setState(AddressChange.State.BEAUFTRAGT);
            AdressaenderungBeauftragt orderRequest = new AdressaenderungBeauftragt(order.getId(), addressChange.getId(), addressChange.getNewAddress());

            kafkaTemplate.send(kafkaTopic, order.getId().toString(), orderRequest);
        }
    }

}
