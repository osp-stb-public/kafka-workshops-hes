package de.osp.training.heskafka.asynctx.erp.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import de.osp.training.heskafka.asynctx.erp.dto.AddressChangeInput;
import de.osp.training.heskafka.asynctx.erp.model.AddressChange;
import de.osp.training.heskafka.asynctx.erp.model.CustomerOrder;
import de.osp.training.heskafka.asynctx.erp.services.AddressChangeService;
import de.osp.training.heskafka.asynctx.erp.services.CustomerOrderService;

@RestController
@RequestMapping("/orders")
public class CustomerOrderController {
    @Autowired
    private AddressChangeService addressChangeService;

    @Autowired
    private CustomerOrderService orderService;

    @PostMapping("/{orderId}/address-changes")
	public AddressChange addAddressChange(@RequestBody AddressChangeInput addressChangeInput, @PathVariable Long orderId) {
        return addressChangeService.addAddressChange(addressChangeInput, orderId);
	}

    @GetMapping("/{orderId}/address-changes")
    public List<AddressChange> getAddressChange(@PathVariable Long orderId) {
        return addressChangeService.getAddressChanges(orderId);
    }

    @GetMapping("/{orderId}/address-changes/{addressChangeId}")
    public AddressChange getAddressChange(@PathVariable Long orderId, @PathVariable Long addressChangeId) {
        return addressChangeService.getAddressChange(orderId, addressChangeId);
    }

    @GetMapping("/{orderId}")
    public CustomerOrder getOrder(@PathVariable Long orderId) {
        return orderService.getOrder(orderId);
    }
}
