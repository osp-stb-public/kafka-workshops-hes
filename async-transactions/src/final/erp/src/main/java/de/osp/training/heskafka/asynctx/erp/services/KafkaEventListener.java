package de.osp.training.heskafka.asynctx.erp.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaHandler;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import de.osp.training.heskafka.asynctx.erp.eventhandlers.AdressaenderungAbgelehntHandler;
import de.osp.training.heskafka.asynctx.erp.eventhandlers.AdressaenderungZugestimmtHandler;
import de.osp.training.heskafka.asynctx.erp.eventhandlers.TimeoutNachAnfrageHandler;
import de.osp.training.heskafka.asynctx.erp.eventhandlers.AdressaenderungAbgebrochenHandler;
import de.osp.training.heskafka.asynctx.erp.eventhandlers.AdressaenderungErfolgtHandler;
import de.osp.training.heskafka.asynctx.erp.eventhandlers.TimeoutNachBeauftragungHandler;
import de.osp.training.heskafka.asynctx.erp.events.AdressaenderungAbgelehnt;
import de.osp.training.heskafka.asynctx.erp.events.AdressaenderungZugestimmt;
import de.osp.training.heskafka.asynctx.erp.events.TimeoutNachAnfrage;
import de.osp.training.heskafka.asynctx.erp.events.AdressaenderungAbgebrochen;
import de.osp.training.heskafka.asynctx.erp.events.AdressaenderungErfolgt;
import de.osp.training.heskafka.asynctx.erp.events.TimeoutNachBeauftragung;

@Service
@KafkaListener(topics = "${KAFKA_TOPIC}", containerFactory = "containerFactory")
public class KafkaEventListener {
    private static final Logger logger = LoggerFactory.getLogger(KafkaEventListener.class);

    @Autowired
    private AdressaenderungZugestimmtHandler adressaenderungZugestimmtHandler;

    @Autowired
    private AdressaenderungAbgelehntHandler adressaenderungAbgelehntHandler;

    @Autowired
    private TimeoutNachAnfrageHandler timeoutNachAnfrageHandler;

    @Autowired
    private AdressaenderungErfolgtHandler adressaenderungErfolgtHandler;

    @Autowired
    private AdressaenderungAbgebrochenHandler adressaenderungAbgebrochenHandler;

    @Autowired
    private TimeoutNachBeauftragungHandler timeoutNachBeauftragungHandler;

    @KafkaHandler
    public void handle(AdressaenderungZugestimmt event) {
        logger.info("Received AdressaenderungZugestimmt: {}", event);
        adressaenderungZugestimmtHandler.handle(event);
    }

    @KafkaHandler
    public void handle(AdressaenderungAbgelehnt event) {
        logger.info("Received AdressaenderungAbgelehnt: {}", event);
        adressaenderungAbgelehntHandler.handle(event);
    }

    @KafkaHandler
    public void handle(TimeoutNachAnfrage event) {
        logger.info("Received TimeoutNachAnfrage: {}", event);
        timeoutNachAnfrageHandler.handle(event);
    }

    @KafkaHandler
    public void handle(TimeoutNachBeauftragung event) {
        logger.info("Received TimeoutNachBeauftragung: {}", event);
        timeoutNachBeauftragungHandler.handle(event);
    }

    @KafkaHandler
    public void handle(AdressaenderungErfolgt event) {
        logger.info("Received AdressaenderungErfolgt: {}", event);
        adressaenderungErfolgtHandler.handle(event);
    }

    @KafkaHandler
    public void handle(AdressaenderungAbgebrochen event) {
        logger.info("Received AdressaenderungAbgebrochen: {}", event);
        adressaenderungAbgebrochenHandler.handle(event);
    }

    @KafkaHandler(isDefault = true)
    public void handleDefault(Object event) {
        // do nothing
    }
}
