package de.osp.training.heskafka.asynctx.erp.events;

public class TimeoutNachBeauftragung extends KafkaEvent {
    public static final String EVENT_TYPE = "TimeoutNachBeauftragung";

    private Long orderId;
    private Long addressChangeId;

    public TimeoutNachBeauftragung() {
        setEventType(EVENT_TYPE);
    }

    public TimeoutNachBeauftragung(Long orderId, Long addressChangeId) {
        setEventType(EVENT_TYPE);
        this.orderId = orderId;
        this.addressChangeId = addressChangeId;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Long getAddressChangeId() {
        return addressChangeId;
    }

    public void setAddressChangeId(Long addressChangeId) {
        this.addressChangeId = addressChangeId;
    }

    @Override
    public String toString() {
        return "TimeoutNachBeauftragung [orderId=" + orderId + ", addressChangeId=" + addressChangeId + "]";
    }

}
