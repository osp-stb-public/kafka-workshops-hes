package de.osp.training.heskafka.asynctx.erp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import de.osp.training.heskafka.asynctx.erp.model.CustomerOrder;

public interface CustomerOrderRepository extends JpaRepository<CustomerOrder, Long> {
}
