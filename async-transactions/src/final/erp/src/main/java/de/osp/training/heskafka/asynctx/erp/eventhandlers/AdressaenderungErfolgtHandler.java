package de.osp.training.heskafka.asynctx.erp.eventhandlers;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import de.osp.training.heskafka.asynctx.erp.events.AdressaenderungAbgeschlossen;
import de.osp.training.heskafka.asynctx.erp.events.AdressaenderungErfolgt;
import de.osp.training.heskafka.asynctx.erp.model.AddressChange;
import de.osp.training.heskafka.asynctx.erp.model.AddressChangeDepotState;
import de.osp.training.heskafka.asynctx.erp.model.CustomerOrder;
import de.osp.training.heskafka.asynctx.erp.repositories.CustomerOrderRepository;

@Service
public class AdressaenderungErfolgtHandler {
    private static final Logger logger = LoggerFactory.getLogger(AdressaenderungErfolgtHandler.class);

    @Autowired
    private CustomerOrderRepository orderRepository;

    @Value("${KAFKA_TOPIC}")
    private String kafkaTopic;

    @Autowired
    private KafkaTemplate<String, Object> kafkaTemplate;

    @Transactional("transactionManager")
    public void handle(AdressaenderungErfolgt event) {
        Optional<CustomerOrder> orderOptional = orderRepository.findById(event.getOrderId());
        
        if (orderOptional.isEmpty()) {
            logger.warn("Order not found with ID: {}", event.getOrderId());
            return;
        }
        
        CustomerOrder order = orderOptional.get();
        Optional<AddressChange> addressChangeOptional = order.getAddressChange(event.getAddressChangeId());

        if (addressChangeOptional.isEmpty()) {
            logger.warn("Address change not found with ID: {}", event.getAddressChangeId());
            return;
        }

        AddressChange addressChange = addressChangeOptional.get();

        if (!addressChange.getState().equals(AddressChange.State.BEAUFTRAGT)) {
            logger.warn("Address change is not in state BEAUFTRAGT");
            return;
        }

        Optional<AddressChangeDepotState> optionalDepotState = addressChange.getDepotState(event.getDepotId());

        if (!optionalDepotState.isPresent()) {
            logger.info("Depot state not found for depotId {}", event.getDepotId());
            return;
        }

        AddressChangeDepotState depotState = optionalDepotState.get();

        if (!depotState.getState().equals(AddressChangeDepotState.State.ZUGESTIMMT)) {
            logger.info("Depot state is not in ZUGESTIMMT for depotId {}", event.getDepotId());
            return;
        }

        depotState.setState(AddressChangeDepotState.State.ERFOLGT);

        if (addressChange.hasAllDepotsChanged()) {
            addressChange.setState(AddressChange.State.DURCHGEFUEHRT);

            order.setAddress(addressChange.getNewAddress());

            AdressaenderungAbgeschlossen commitEvent = new AdressaenderungAbgeschlossen(order.getId(), addressChange.getId());
            kafkaTemplate.send(kafkaTopic, order.getId().toString(), commitEvent);
        }
    }

}
