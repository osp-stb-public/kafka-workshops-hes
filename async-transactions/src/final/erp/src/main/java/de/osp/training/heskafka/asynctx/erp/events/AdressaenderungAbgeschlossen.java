package de.osp.training.heskafka.asynctx.erp.events;

public class AdressaenderungAbgeschlossen extends KafkaEvent {
    public static final String EVENT_TYPE = "AdressaenderungAbgeschlossen";

    private Long orderId;
    private Long addressChangeId;

    public AdressaenderungAbgeschlossen() {
        setEventType(EVENT_TYPE);
    }

    public AdressaenderungAbgeschlossen(Long orderId, Long addressChangeId) {
        setEventType(EVENT_TYPE);
        this.orderId = orderId;
        this.addressChangeId = addressChangeId;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Long getAddressChangeId() {
        return addressChangeId;
    }

    public void setAddressChangeId(Long addressChangeId) {
        this.addressChangeId = addressChangeId;
    }

    @Override
    public String toString() {
        return "AdressaenderungAbgeschlossen [orderId=" + orderId + ", addressChangeId=" + addressChangeId + "]";
    }

    
}
