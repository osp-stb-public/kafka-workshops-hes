package de.osp.training.heskafka.asynctx.erp.events;

public class AdressaenderungErfolgt extends KafkaEvent {
    public static final String EVENT_TYPE = "AdressaenderungErfolgt";

    private Long depotId;
    private Long orderId;
    private Long addressChangeId;

    public AdressaenderungErfolgt() {
        setEventType(EVENT_TYPE);
    }

    public AdressaenderungErfolgt(Long depotId, Long orderId, Long addressChangeId) {
        setEventType(EVENT_TYPE);
        this.depotId = depotId;
        this.orderId = orderId;
        this.addressChangeId = addressChangeId;
    }

    public Long getDepotId() {
        return depotId;
    }

    public void setDepotId(Long depotId) {
        this.depotId = depotId;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Long getAddressChangeId() {
        return addressChangeId;
    }

    public void setAddressChangeId(Long addressChangeId) {
        this.addressChangeId = addressChangeId;
    }

    @Override
    public String toString() {
        return "AdressaenderungErfolgt [depotId=" + depotId + ", orderId=" + orderId + ", addressChangeId="
                + addressChangeId + "]";
    }

}
