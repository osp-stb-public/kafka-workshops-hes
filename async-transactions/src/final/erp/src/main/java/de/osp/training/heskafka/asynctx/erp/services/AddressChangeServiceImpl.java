package de.osp.training.heskafka.asynctx.erp.services;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import de.osp.training.heskafka.asynctx.erp.dto.AddressChangeInput;
import de.osp.training.heskafka.asynctx.erp.events.AdressaenderungAngefragt;
import de.osp.training.heskafka.asynctx.erp.exceptions.AddressChangeNotFoundException;
import de.osp.training.heskafka.asynctx.erp.exceptions.CustomerOrderNotFoundException;
import de.osp.training.heskafka.asynctx.erp.model.Address;
import de.osp.training.heskafka.asynctx.erp.model.AddressChange;
import de.osp.training.heskafka.asynctx.erp.model.AddressChangeDepotState;
import de.osp.training.heskafka.asynctx.erp.model.CustomerOrder;
import de.osp.training.heskafka.asynctx.erp.repositories.AddressChangeRepository;
import de.osp.training.heskafka.asynctx.erp.repositories.CustomerOrderRepository;

@Service
public class AddressChangeServiceImpl implements AddressChangeService {
    private static final Logger logger = LoggerFactory.getLogger(AddressChangeServiceImpl.class);
    
    @Autowired
    private CustomerOrderRepository orderRepository;

    @Autowired
    private AddressChangeRepository addressChangeRepository;

    @Autowired
    private DepotService depotService;

    @Value("${KAFKA_TOPIC}")
    private String kafkaTopic;

    @Autowired
    private KafkaTemplate<String, Object> kafkaTemplate;

    @Override
    @Transactional("transactionManager")
    public AddressChange addAddressChange(AddressChangeInput addressChangeInput, Long orderId) {
        CustomerOrder order = orderRepository.findById(orderId)
                .orElseThrow(() -> new CustomerOrderNotFoundException(orderId));

        Address address = createAddress(addressChangeInput);
        AddressChange addressChange = createAddressChange(address);

        order.addAddressChange(addressChange);

        logger.info("AddressChange created: {}", addressChange);

        validateAddressChange(addressChange);

        sendAddressChangeApprovalRequest(addressChange);

        return addressChange;
    }

    private Address createAddress(AddressChangeInput input) {
        Address address = new Address();
        address.setName(input.getNewAddress().getName());
        address.setStreet(input.getNewAddress().getStreet());
        address.setCity(input.getNewAddress().getCity());
        address.setZip(input.getNewAddress().getZip());
        return address;
    }

    private AddressChange createAddressChange(Address address) {
        AddressChange addressChange = new AddressChange();
        addressChange.setNewAddress(address);
        addressChange.setState(AddressChange.State.EINGEGANGEN);
        return addressChange;
    }

    private void validateAddressChange(AddressChange addressChange) {
        // here we could add some validation logic
    }

    private void sendAddressChangeApprovalRequest(AddressChange addressChange) {
        List<Long> depots = depotService.getAllDepots();

        // create depot state list
        depots.stream().map(depotId -> {
            AddressChangeDepotState depotState = new AddressChangeDepotState();
            depotState.setDepotId(depotId);
            depotState.setState(AddressChangeDepotState.State.WARTEN_AUF_ZUSTIMMUNG);
            return depotState;
        }).forEach(addressChange::addDepotState);

        addressChange.setState(AddressChange.State.WARTEN_AUF_ZUSTIMUNG_ALLER_SUBMODULE);

        CustomerOrder order = addressChange.getOrder();

        addressChangeRepository.save(addressChange); // necessary to auto asign id to addressChange

        AdressaenderungAngefragt approvalRequest = new AdressaenderungAngefragt(order.getId(), addressChange.getId());

        kafkaTemplate.send(kafkaTopic, order.getId().toString(), approvalRequest);
    }

    @Override
    public List<AddressChange> getAddressChanges(Long orderId) {
        CustomerOrder order = orderRepository.findById(orderId)
                .orElseThrow(() -> new CustomerOrderNotFoundException(orderId));

        return order.getAddressChanges();
    }

    @Override
    public AddressChange getAddressChange(Long orderId, Long addressChangeId) {
        CustomerOrder order = orderRepository.findById(orderId)
                .orElseThrow(() -> new CustomerOrderNotFoundException(orderId));

        return order.getAddressChange(addressChangeId)
                .orElseThrow(() -> new AddressChangeNotFoundException(orderId, addressChangeId));
    }
}
