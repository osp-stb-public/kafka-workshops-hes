package de.osp.training.heskafka.asynctx.erp.events;

public class AdressaenderungAbgelehnt extends KafkaEvent {
    public static final String EVENT_TYPE = "AdressaenderungAbgelehnt";

    private Long depotId;
    private Long orderId;
    private Long addressChangeId;

    public AdressaenderungAbgelehnt() {
        setEventType(EVENT_TYPE);
    }

    public AdressaenderungAbgelehnt(Long depotId, Long orderId, Long addressChangeId) {
        setEventType(EVENT_TYPE);
        this.depotId = depotId;
        this.orderId = orderId;
        this.addressChangeId = addressChangeId;
    }

    public Long getDepotId() {
        return depotId;
    }

    public void setDepotId(Long depotId) {
        this.depotId = depotId;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Long getAddressChangeId() {
        return addressChangeId;
    }

    public void setAddressChangeId(Long addressChangeId) {
        this.addressChangeId = addressChangeId;
    }

    @Override
    public String toString() {
        return "AdressaenderungAbgelehnt [depotId=" + depotId + ", orderId=" + orderId + ", addressChangeId="
                + addressChangeId + "]";
    }

}
