package de.osp.training.heskafka.asynctx.erp.dto;

public class ExceptionOutput {
    private String error;

    public ExceptionOutput(Exception exception) {
        this.error = exception.getMessage();
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

}
