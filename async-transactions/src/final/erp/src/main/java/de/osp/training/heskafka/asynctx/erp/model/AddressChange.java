package de.osp.training.heskafka.asynctx.erp.model;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.fasterxml.jackson.annotation.JsonBackReference;

import jakarta.persistence.ElementCollection;
import jakarta.persistence.Embedded;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;

@Entity
public class AddressChange {
    @Id @GeneratedValue
    private Long id;
    @Embedded
    private Address newAddress;
    @Enumerated(EnumType.STRING)
    private State state;
    private Instant lastStateChange;
    @ManyToOne
    @JoinColumn(name = "customerorder_id", nullable = false)
    @JsonBackReference
    private CustomerOrder order;
    @ElementCollection
    private List<AddressChangeDepotState> depotStates = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Address getNewAddress() {
        return newAddress;
    }

    public void setNewAddress(Address newAddress) {
        this.newAddress = newAddress;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        setLastStateChange(Instant.now());
        this.state = state;
    }

    public Instant getLastStateChange() {
        return lastStateChange;
    }

    public void setLastStateChange(Instant lastStateChange) {
        this.lastStateChange = lastStateChange;
    }

    public List<AddressChangeDepotState> getDepotStates() {
        return depotStates;
    }

    public void addDepotState(AddressChangeDepotState depotState) {
        this.depotStates.add(depotState);
    }

    public Optional<AddressChangeDepotState> getDepotState(Long depotId) {
        return depotStates.stream().filter(ds -> ds.getDepotId().equals(depotId)).findFirst();
    }

    public CustomerOrder getOrder() {
        return order;
    }

    public void setOrder(CustomerOrder order) {
        this.order = order;
    }
    
    public boolean isOngoing() {
        return state == State.EINGEGANGEN || state == State.WARTEN_AUF_ZUSTIMUNG_ALLER_SUBMODULE
                || state == State.BEAUFTRAGT;
    }

    public boolean hasAllDepotsApproved() {
        return depotStates.stream().allMatch(depotState -> depotState.getState() == AddressChangeDepotState.State.ZUGESTIMMT);
    }

    public boolean hasAllDepotsChanged() {
        return depotStates.stream().allMatch(depotState -> depotState.getState() == AddressChangeDepotState.State.ERFOLGT);
    }

    public enum State {
        EINGEGANGEN,
        WARTEN_AUF_ZUSTIMUNG_ALLER_SUBMODULE,
        ABGELEHNT,
        BEAUFTRAGT,
        ABGEBROCHEN,
        DURCHGEFUEHRT,
    }

    @Override
    public String toString() {
        return "AddressChange [id=" + id + ", newAddress=" + newAddress + ", state=" + state + ", lastStateChange="
                + lastStateChange + ", depotStates=" + depotStates + "]";
    }

}
