package de.osp.training.heskafka.asynctx.erp.events;

import de.osp.training.heskafka.asynctx.erp.model.Address;

public class AdressaenderungBeauftragt extends KafkaEvent {
    public static final String EVENT_TYPE = "AdressaenderungBeauftragt";

    private Long orderId;
    private Long addressChangeId;
    private Address newAddress;

    public AdressaenderungBeauftragt() {
        setEventType(EVENT_TYPE);
    }

    public AdressaenderungBeauftragt(Long orderId, Long addressChangeId, Address newAddress) {
        setEventType(EVENT_TYPE);
        this.orderId = orderId;
        this.addressChangeId = addressChangeId;
        this.newAddress = newAddress;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Long getAddressChangeId() {
        return addressChangeId;
    }

    public void setAddressChangeId(Long addressChangeId) {
        this.addressChangeId = addressChangeId;
    }

    public Address getNewAddress() {
        return newAddress;
    }

    public void setNewAddress(Address newAddress) {
        this.newAddress = newAddress;
    }

    @Override
    public String toString() {
        return "AdressaenderungBeauftragt [orderId=" + orderId + ", addressChangeId=" + addressChangeId
                + ", newAddress=" + newAddress + "]";
    }

}
