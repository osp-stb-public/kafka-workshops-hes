package de.osp.training.heskafka.asynctx.erp.exceptionhandlers;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import de.osp.training.heskafka.asynctx.erp.dto.ExceptionOutput;
import de.osp.training.heskafka.asynctx.erp.exceptions.AddressChangeOngoingException;

@RestControllerAdvice
class AddressChangeOngoingAdvice {

    @ExceptionHandler(AddressChangeOngoingException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ExceptionOutput addressChangeOngoingHandler(AddressChangeOngoingException ex) {
        return new ExceptionOutput(ex);
    }

}