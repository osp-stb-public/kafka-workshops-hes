package de.osp.training.heskafka.asynctx.erp.model;

import java.util.List;
import java.util.Optional;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import de.osp.training.heskafka.asynctx.erp.exceptions.AddressChangeOngoingException;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Embedded;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;

@Entity
public class CustomerOrder {
    @Id @GeneratedValue
    private Long id;
    @Embedded
    private Address address;
    @OneToMany(mappedBy = "order", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
    @JsonManagedReference
    private List<AddressChange> addressChanges;

    public CustomerOrder() {
    }

    public CustomerOrder(Address address, List<AddressChange> addressChanges) {
        this.address = address;
        this.addressChanges = addressChanges;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public List<AddressChange> getAddressChanges() {
        return addressChanges;
    }

    public Optional<AddressChange> getAddressChange(Long addressChangeId) {
        return addressChanges.stream().filter(ac -> ac.getId().equals(addressChangeId)).findFirst();
    }

    public void addAddressChange(AddressChange addressChange) {
        if (hasOngoingAddressChange()) {
            throw new AddressChangeOngoingException(id, addressChange.getId());
        }

        addressChange.setOrder(this);

        addressChanges.add(addressChange);
    }

    public boolean hasOngoingAddressChange() {
        return this.addressChanges.stream().anyMatch(AddressChange::isOngoing);
    }

    @Override
    public String toString() {
        return "CustomerOrder [id=" + id + ", address=" + address + ", addressChanges=" + addressChanges + "]";
    }

}
