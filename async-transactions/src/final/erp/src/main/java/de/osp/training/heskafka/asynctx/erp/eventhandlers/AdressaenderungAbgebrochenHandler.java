package de.osp.training.heskafka.asynctx.erp.eventhandlers;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import de.osp.training.heskafka.asynctx.erp.events.AdressaenderungAbgebrochen;
import de.osp.training.heskafka.asynctx.erp.model.AddressChange;
import de.osp.training.heskafka.asynctx.erp.model.AddressChangeDepotState;
import de.osp.training.heskafka.asynctx.erp.model.CustomerOrder;
import de.osp.training.heskafka.asynctx.erp.repositories.CustomerOrderRepository;

@Service
public class AdressaenderungAbgebrochenHandler {
    private static final Logger logger = LoggerFactory.getLogger(AdressaenderungAbgebrochenHandler.class);

    @Autowired
    private CustomerOrderRepository orderRepository;

    @Transactional("transactionManager")
    public void handle(AdressaenderungAbgebrochen event) {
        Optional<CustomerOrder> orderOptional = orderRepository.findById(event.getOrderId());
        
        if (orderOptional.isEmpty()) {
            logger.warn("Order not found with ID: {}", event.getOrderId());
            return;
        }
        
        CustomerOrder order = orderOptional.get();
        Optional<AddressChange> addressChangeOptional = order.getAddressChange(event.getAddressChangeId());

        if (addressChangeOptional.isEmpty()) {
            logger.warn("Address change not found with ID: {}", event.getAddressChangeId());
            return;
        }

        AddressChange addressChange = addressChangeOptional.get();

        if (!addressChange.getState().equals(AddressChange.State.BEAUFTRAGT)) {
            logger.warn("Address change is not in state BEAUFTRAGT");
            return;
        }

        Optional<AddressChangeDepotState> optionalDepotState = addressChange.getDepotState(event.getDepotId());

        if (!optionalDepotState.isPresent()) {
            logger.info("Depot state not found for depotId {}", event.getDepotId());
            return;
        }

        AddressChangeDepotState depotState = optionalDepotState.get();

        if (!depotState.getState().equals(AddressChangeDepotState.State.ZUGESTIMMT)) {
            logger.info("Depot state is not in ZUGESTIMMT for depotId {}", event.getDepotId());
            return;
        }

        depotState.setState(AddressChangeDepotState.State.ABGEBROCHEN);

        addressChange.setState(AddressChange.State.ABGEBROCHEN);
    }
}
