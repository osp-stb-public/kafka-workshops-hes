package de.osp.training.heskafka.asynctx.erp.services;

import de.osp.training.heskafka.asynctx.erp.model.CustomerOrder;

public interface CustomerOrderService {
    public CustomerOrder getOrder(Long orderId);
}
