package de.osp.training.heskafka.asynctx.submodule.services;

import de.osp.training.heskafka.asynctx.submodule.model.CustomerOrder;

public interface CustomerOrderService {
    public CustomerOrder getOrder(Long orderId);
}
