package de.osp.training.heskafka.asynctx.submodule.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import de.osp.training.heskafka.asynctx.submodule.model.CustomerOrder;
import de.osp.training.heskafka.asynctx.submodule.services.CustomerOrderService;

@RestController
@RequestMapping("/orders")
public class CustomerOrderController {
    @Autowired
    private CustomerOrderService orderService;

    @GetMapping("/{orderId}")
    public CustomerOrder getOrder(@PathVariable Long orderId) {
        return orderService.getOrder(orderId);
    }
}
