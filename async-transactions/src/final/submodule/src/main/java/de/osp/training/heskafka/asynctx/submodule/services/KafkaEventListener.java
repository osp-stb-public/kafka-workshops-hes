package de.osp.training.heskafka.asynctx.submodule.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaHandler;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import de.osp.training.heskafka.asynctx.submodule.eventhandlers.AdressaenderungAngefragtHandler;
import de.osp.training.heskafka.asynctx.submodule.eventhandlers.AdressaenderungAbgebrochenHandler;
import de.osp.training.heskafka.asynctx.submodule.eventhandlers.AdressaenderungAbgeschlossenHandler;
import de.osp.training.heskafka.asynctx.submodule.eventhandlers.AdressaenderungBeauftragtHandler;
import de.osp.training.heskafka.asynctx.submodule.events.AdressaenderungAbgebrochen;
import de.osp.training.heskafka.asynctx.submodule.events.AdressaenderungAbgeschlossen;
import de.osp.training.heskafka.asynctx.submodule.events.AdressaenderungAngefragt;
import de.osp.training.heskafka.asynctx.submodule.events.AdressaenderungBeauftragt;

@Service
@KafkaListener(topics = "${KAFKA_TOPIC}", containerFactory = "containerFactory")
public class KafkaEventListener {
    private static final Logger logger = LoggerFactory.getLogger(KafkaEventListener.class);

    @Autowired
    private AdressaenderungAngefragtHandler adressaenderungAngefragtHandler;

    @Autowired
    private AdressaenderungBeauftragtHandler adressaenderungBeauftragtHandler;

    @Autowired
    private AdressaenderungAbgeschlossenHandler adressaenderungAbgeschlossenHandler;

    @Autowired
    private AdressaenderungAbgebrochenHandler adressaenderungAbgebrochenHandler;


    @KafkaHandler
    public void handle(AdressaenderungAngefragt event) {
        logger.info("Received AdressaenderungAngefragt: {}", event);
        adressaenderungAngefragtHandler.handle(event);
    }

    @KafkaHandler
    public void handle(AdressaenderungBeauftragt event) {
        logger.info("Received AdressaenderungBeauftragt: {}", event);
        adressaenderungBeauftragtHandler.handle(event);
    }

    @KafkaHandler
    public void handle(AdressaenderungAbgeschlossen event) {
        logger.info("Received AdressaenderungAbgeschlossen: {}", event);
        adressaenderungAbgeschlossenHandler.handle(event);
    }

    @KafkaHandler
    public void handle(AdressaenderungAbgebrochen event) {
        logger.info("Received AdressaenderungAbgebrochen: {}", event);
        adressaenderungAbgebrochenHandler.handle(event);
    }

    @KafkaHandler(isDefault = true)
    public void handleDefault(Object event) {
        // do nothing
    }
}
