package de.osp.training.heskafka.asynctx.submodule.model;

import java.util.List;
import java.util.Optional;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;

@Entity
public class CustomerOrder {
    @Id
    @GeneratedValue
    private Long id;
    @OneToMany(mappedBy = "order", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
    @JsonManagedReference
    private List<AddressChange> addressChanges;

    public CustomerOrder() {
    }

    public CustomerOrder(List<AddressChange> addressChanges) {
        this.addressChanges = addressChanges;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Address getLatestAddress() {
        return addressChanges.stream()
                .filter(ac -> ac.getState().equals(AddressChange.State.DURCHGEFÜHRT))
                .sorted((ac1, ac2) -> ac2.getId().compareTo(ac1.getId()))
                .findFirst()
                .map(AddressChange::getAddress)
                .orElse(null);
    }

    public List<AddressChange> getAddressChanges() {
        return addressChanges;
    }

    public Optional<AddressChange> getAddressChange(Long addressChangeId) {
        return addressChanges.stream().filter(ac -> ac.getId().equals(addressChangeId)).findFirst();
    }

    public void addAddressChange(AddressChange addressChange) {
        addressChange.setOrder(this);

        addressChanges.add(addressChange);
    }

    @Override
    public String toString() {
        return "CustomerOrder [id=" + id + ", addressChanges=" + addressChanges + "]";
    }

}
