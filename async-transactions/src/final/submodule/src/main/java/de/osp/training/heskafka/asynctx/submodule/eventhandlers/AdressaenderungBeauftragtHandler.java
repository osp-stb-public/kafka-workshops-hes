package de.osp.training.heskafka.asynctx.submodule.eventhandlers;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import de.osp.training.heskafka.asynctx.submodule.events.AdressaenderungAbgebrochen;
import de.osp.training.heskafka.asynctx.submodule.events.AdressaenderungBeauftragt;
import de.osp.training.heskafka.asynctx.submodule.events.AdressaenderungErfolgt;
import de.osp.training.heskafka.asynctx.submodule.events.KafkaEvent;
import de.osp.training.heskafka.asynctx.submodule.model.AddressChange;
import de.osp.training.heskafka.asynctx.submodule.model.CustomerOrder;
import de.osp.training.heskafka.asynctx.submodule.repositories.CustomerOrderRepository;
import de.osp.training.heskafka.asynctx.submodule.services.DemoService;

@Service
public class AdressaenderungBeauftragtHandler {
    private static final Logger logger = LoggerFactory.getLogger(AdressaenderungBeauftragtHandler.class);

    @Value("${DEPOT_ID}")
    private Long depotId;

    @Value("${KAFKA_TOPIC}")
    private String kafkaTopic;

    @Autowired
    private KafkaTemplate<String, Object> kafkaTemplate;

    @Autowired
    private CustomerOrderRepository orderRepository;

    @Autowired
    private DemoService demoService;

    @Transactional("transactionManager")
    public void handle(AdressaenderungBeauftragt event) {
        demoService.doSomeStuffOnAdressaenderungBeauftragt(event);

        Optional<CustomerOrder> orderOptional = orderRepository.findById(event.getOrderId());
        
        if (orderOptional.isEmpty()) {
            logger.warn("Order not found with ID: {}", event.getOrderId());
            return;
        }

        CustomerOrder order = orderOptional.get();

        Optional<AddressChange> addressChangeOptional = order.getAddressChange(event.getAddressChangeId());

        if (!addressChangeOptional.isEmpty()) {
            logger.warn("Address change already exists with ID: {}", event.getAddressChangeId());
            return;
        }

        AddressChange addressChange = new AddressChange(event.getAddressChangeId(), event.getNewAddress());
        order.addAddressChange(addressChange);

        KafkaEvent reponseEvent;

        if (demoService.sendAdressaenderungErfolgt(event)) {
            reponseEvent = new AdressaenderungErfolgt(depotId, event.getOrderId(), event.getAddressChangeId());
        } else {
            reponseEvent = new AdressaenderungAbgebrochen(depotId, event.getOrderId(), event.getAddressChangeId());
        }

        kafkaTemplate.send(kafkaTopic, order.getId().toString(), reponseEvent);
    }
}
