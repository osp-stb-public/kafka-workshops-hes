package de.osp.training.heskafka.asynctx.submodule.eventhandlers;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import de.osp.training.heskafka.asynctx.submodule.events.AdressaenderungAbgebrochen;
import de.osp.training.heskafka.asynctx.submodule.model.AddressChange;
import de.osp.training.heskafka.asynctx.submodule.model.CustomerOrder;
import de.osp.training.heskafka.asynctx.submodule.repositories.CustomerOrderRepository;

@Service
public class AdressaenderungAbgebrochenHandler {
    private static final Logger logger = LoggerFactory.getLogger(AdressaenderungAbgebrochenHandler.class);

    @Autowired
    private CustomerOrderRepository orderRepository;

    @Transactional("transactionManager")
    public void handle(AdressaenderungAbgebrochen event) {
        Optional<CustomerOrder> orderOptional = orderRepository.findById(event.getOrderId());
        
        if (orderOptional.isEmpty()) {
            logger.warn("Order not found with ID: {}", event.getOrderId());
            return;
        }
        
        CustomerOrder order = orderOptional.get();
        Optional<AddressChange> addressChangeOptional = order.getAddressChange(event.getAddressChangeId());

        if (addressChangeOptional.isEmpty()) {
            logger.warn("Address change not found with ID: {}", event.getAddressChangeId());
            return;
        }

        AddressChange addressChange = addressChangeOptional.get();

        if (!addressChange.getState().equals(AddressChange.State.WARTEN_AUF_BESTÄTIGUNG)) {
            logger.warn("Address change is not in state WARTEN_AUF_BESTÄTIGUNG");
            return;
        }

        addressChange.setState(AddressChange.State.ABGEBROCHEN);
    }
}
