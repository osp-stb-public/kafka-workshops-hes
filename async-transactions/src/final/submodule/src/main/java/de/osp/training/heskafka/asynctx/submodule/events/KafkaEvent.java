package de.osp.training.heskafka.asynctx.submodule.events;

import java.time.Instant;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.EXISTING_PROPERTY, property = "eventType")
@JsonSubTypes({
        @JsonSubTypes.Type(value = AdressaenderungAbgebrochen.class, name = "AdressaenderungAbgebrochen"),
        @JsonSubTypes.Type(value = AdressaenderungAbgelehnt.class, name = "AdressaenderungAbgelehnt"),
        @JsonSubTypes.Type(value = AdressaenderungAbgeschlossen.class, name = "AdressaenderungAbgeschlossen"),
        @JsonSubTypes.Type(value = AdressaenderungAngefragt.class, name = "AdressaenderungAngefragt"),
        @JsonSubTypes.Type(value = AdressaenderungBeauftragt.class, name = "AdressaenderungBeauftragt"),
        @JsonSubTypes.Type(value = AdressaenderungErfolgt.class, name = "AdressaenderungErfolgt"),
        @JsonSubTypes.Type(value = AdressaenderungZugestimmt.class, name = "AdressaenderungZugestimmt"),
        @JsonSubTypes.Type(value = TimeoutNachAnfrage.class, name = "TimeoutNachAnfrage"),
        @JsonSubTypes.Type(value = TimeoutNachBeauftragung.class, name = "TimeoutNachBeauftragung"),
})
public abstract class KafkaEvent {
    private UUID eventId = UUID.randomUUID();
    private UUID traceId = UUID.randomUUID();
    private String eventTime = Instant.now().toString(); // use String to simplify serialization
    private String eventType;

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public UUID getEventId() {
        return eventId;
    }

    public void setEventId(UUID eventId) {
        this.eventId = eventId;
    }

    public UUID getTraceId() {
        return traceId;
    }

    public void setTraceId(UUID traceId) {
        this.traceId = traceId;
    }

    public String getEventTime() {
        return eventTime;
    }

    public void setEventTime(String eventTime) {
        this.eventTime = eventTime;
    }

    @Override
    public String toString() {
        return "KafkaEvent [eventId=" + eventId + ", traceId=" + traceId + ", eventTime=" + eventTime + ", eventType="
                + eventType + "]";
    }

}
