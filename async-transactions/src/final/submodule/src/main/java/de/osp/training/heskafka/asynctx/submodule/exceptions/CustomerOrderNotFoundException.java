package de.osp.training.heskafka.asynctx.submodule.exceptions;

public class CustomerOrderNotFoundException extends RuntimeException {

    public CustomerOrderNotFoundException(Long orderId) {
        super("Could not find order " + orderId);
    }

}
