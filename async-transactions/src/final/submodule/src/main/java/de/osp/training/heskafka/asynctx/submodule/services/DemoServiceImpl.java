package de.osp.training.heskafka.asynctx.submodule.services;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import de.osp.training.heskafka.asynctx.submodule.events.AdressaenderungAngefragt;
import de.osp.training.heskafka.asynctx.submodule.events.AdressaenderungBeauftragt;

@Service
public class DemoServiceImpl implements DemoService {
    @Value("${DEPOT_ID}")
    private Long depotId;

    // depotId = 1 && orderId = 1: Happy Path
    // depotId = 1 && orderId = 2: Timeout während Anfrage
    // depotId = 1 && orderId = 3: Anfrage abgelehnt
    // depotId = 1 && orderId = 4: Timeout während Beauftragung
    // depotId = 1 && orderId = 5: Änderung abgebrochen

    @Override
    public void doSomeStuffOnAdressaenderungAngefragt(AdressaenderungAngefragt event) {
        if (depotId == 1 && event.getOrderId() == 2) {
            try {
                Thread.sleep(20000);
            } catch (InterruptedException e) {
                throw new RuntimeException("Error sleeping", e);
            }
        }
    }

    @Override
    public boolean sendAdressaenderungZugestimmt(AdressaenderungAngefragt event) {
        if (depotId == 1 && event.getOrderId() == 3) {
            return false;
        }

        return true;
    }

    @Override
    public void doSomeStuffOnAdressaenderungBeauftragt(AdressaenderungBeauftragt event) {
        if (depotId == 1 && event.getOrderId() == 4) {
            try {
                Thread.sleep(20000);
            } catch (InterruptedException e) {
                throw new RuntimeException("Error sleeping", e);
            }
        }
    }

    @Override
    public boolean sendAdressaenderungErfolgt(AdressaenderungBeauftragt event) {
        if (depotId == 1 && event.getOrderId() == 5) {
            return false;
        }

        return true;
    }

}
