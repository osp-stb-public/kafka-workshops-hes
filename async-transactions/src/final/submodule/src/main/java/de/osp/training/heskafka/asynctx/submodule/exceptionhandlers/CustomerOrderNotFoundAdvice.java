package de.osp.training.heskafka.asynctx.submodule.exceptionhandlers;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import de.osp.training.heskafka.asynctx.submodule.dto.ExceptionOutput;
import de.osp.training.heskafka.asynctx.submodule.exceptions.CustomerOrderNotFoundException;

@RestControllerAdvice
class CustomerOrderNotFoundAdvice {

    @ExceptionHandler(CustomerOrderNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ExceptionOutput orderNotFoundHandler(CustomerOrderNotFoundException ex) {
        return new ExceptionOutput(ex);
    }

}