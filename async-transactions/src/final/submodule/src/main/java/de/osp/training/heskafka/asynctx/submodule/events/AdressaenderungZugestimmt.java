package de.osp.training.heskafka.asynctx.submodule.events;

public class AdressaenderungZugestimmt extends KafkaEvent {
    public static final String EVENT_TYPE = "AdressaenderungZugestimmt";

    private Long depotId;
    private Long orderId;
    private Long addressChangeId;

    public AdressaenderungZugestimmt() {
        setEventType(EVENT_TYPE);
    }

    public AdressaenderungZugestimmt(Long depotId, Long orderId, Long addressChangeId) {
        setEventType(EVENT_TYPE);
        this.depotId = depotId;
        this.orderId = orderId;
        this.addressChangeId = addressChangeId;
    }

    public Long getDepotId() {
        return depotId;
    }

    public void setDepotId(Long depotId) {
        this.depotId = depotId;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Long getAddressChangeId() {
        return addressChangeId;
    }

    public void setAddressChangeId(Long addressChangeId) {
        this.addressChangeId = addressChangeId;
    }

    @Override
    public String toString() {
        return "AdressaenderungZugestimmt [depotId=" + depotId + ", orderId=" + orderId + ", addressChangeId="
                + addressChangeId + "]";
    }

}
