package de.osp.training.heskafka.asynctx.submodule.events;

public class TimeoutNachAnfrage extends KafkaEvent {
    public static final String EVENT_TYPE = "TimeoutNachAnfrage";

    private Long orderId;
    private Long addressChangeId;

    public TimeoutNachAnfrage() {
        setEventType(EVENT_TYPE);
    }

    public TimeoutNachAnfrage(Long orderId, Long addressChangeId) {
        setEventType(EVENT_TYPE);
        this.orderId = orderId;
        this.addressChangeId = addressChangeId;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Long getAddressChangeId() {
        return addressChangeId;
    }

    public void setAddressChangeId(Long addressChangeId) {
        this.addressChangeId = addressChangeId;
    }

    @Override
    public String toString() {
        return "TimeoutNachAnfrage [orderId=" + orderId + ", addressChangeId=" + addressChangeId + "]";
    }

}
