package de.osp.training.heskafka.asynctx.submodule.eventhandlers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import de.osp.training.heskafka.asynctx.submodule.events.AdressaenderungAbgelehnt;
import de.osp.training.heskafka.asynctx.submodule.events.AdressaenderungAngefragt;
import de.osp.training.heskafka.asynctx.submodule.events.AdressaenderungZugestimmt;
import de.osp.training.heskafka.asynctx.submodule.events.KafkaEvent;
import de.osp.training.heskafka.asynctx.submodule.services.DemoService;

@Service
public class AdressaenderungAngefragtHandler {
    @Value("${DEPOT_ID}")
    private Long depotId;

    @Value("${KAFKA_TOPIC}")
    private String kafkaTopic;

    @Autowired
    private KafkaTemplate<String, Object> kafkaTemplate;

    @Autowired
    private DemoService demoService;

    @Transactional("transactionManager")
    public void handle(AdressaenderungAngefragt event) {
        demoService.doSomeStuffOnAdressaenderungAngefragt(event);

        KafkaEvent reponseEvent;
        
        if (demoService.sendAdressaenderungZugestimmt(event)) {
            reponseEvent = new AdressaenderungZugestimmt(depotId, event.getOrderId(), event.getAddressChangeId());
        } else {
            reponseEvent = new AdressaenderungAbgelehnt(depotId, event.getOrderId(), event.getAddressChangeId());
        } 

        kafkaTemplate.send(kafkaTopic, event.getOrderId().toString(), reponseEvent);
    }
}
