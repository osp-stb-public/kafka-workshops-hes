package de.osp.training.heskafka.asynctx.erp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import de.osp.training.heskafka.asynctx.erp.model.AddressChange;

public interface AddressChangeRepository extends JpaRepository<AddressChange, Long> {
}
