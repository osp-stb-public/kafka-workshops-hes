package de.osp.training.heskafka.asynctx.erp.dto;

public class AddressChangeInput {
    private AddressInput newAddress;

    public AddressInput getNewAddress() {
        return newAddress;
    }

    public void setNewAddress(AddressInput newAddress) {
        this.newAddress = newAddress;
    }

}
