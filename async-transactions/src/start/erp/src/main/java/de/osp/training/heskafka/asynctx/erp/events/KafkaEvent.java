package de.osp.training.heskafka.asynctx.erp.events;

import java.time.Instant;
import java.util.UUID;

public abstract class KafkaEvent {
    private UUID eventId = UUID.randomUUID();
    private UUID traceId = UUID.randomUUID();
    private String eventTime = Instant.now().toString(); // use String to simplify serialization
    private String eventType;

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public UUID getEventId() {
        return eventId;
    }

    public void setEventId(UUID eventId) {
        this.eventId = eventId;
    }

    public UUID getTraceId() {
        return traceId;
    }

    public void setTraceId(UUID traceId) {
        this.traceId = traceId;
    }

    public String getEventTime() {
        return eventTime;
    }

    public void setEventTime(String eventTime) {
        this.eventTime = eventTime;
    }

    @Override
    public String toString() {
        return "KafkaEvent [eventId=" + eventId + ", traceId=" + traceId + ", eventTime=" + eventTime + ", eventType="
                + eventType + "]";
    }

}
