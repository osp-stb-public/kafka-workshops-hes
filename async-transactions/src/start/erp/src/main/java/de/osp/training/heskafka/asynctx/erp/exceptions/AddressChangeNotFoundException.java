package de.osp.training.heskafka.asynctx.erp.exceptions;

public class AddressChangeNotFoundException extends RuntimeException {

    public AddressChangeNotFoundException(Long orderId, Long addressChangeId) {
        super("Could not find address change " + addressChangeId + " for order " + orderId);
    }

}
