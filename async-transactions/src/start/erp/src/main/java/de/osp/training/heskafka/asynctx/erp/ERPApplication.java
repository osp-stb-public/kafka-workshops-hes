package de.osp.training.heskafka.asynctx.erp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class ERPApplication {

	public static void main(String[] args) {
		SpringApplication.run(ERPApplication.class, args);
	}

}
