package de.osp.training.heskafka.asynctx.erp.model;

import jakarta.persistence.Embeddable;

@Embeddable
public class Address {
    private String name;
    private String street;
    private String zip;
    private String city;

    public Address() {
    }

    public Address(String name, String street, String zip, String city) {
        this.name = name;
        this.street = street;
        this.zip = zip;
        this.city = city;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public String toString() {
        return "Address [name=" + name + ", street=" + street + ", zip=" + zip + ", city=" + city + "]";
    }

    
}
