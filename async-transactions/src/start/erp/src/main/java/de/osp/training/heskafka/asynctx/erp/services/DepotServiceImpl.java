package de.osp.training.heskafka.asynctx.erp.services;

import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class DepotServiceImpl implements DepotService {
    // return fixed list of depots for demonstration purposes
    public List<Long> getAllDepots() {
        return List.of(1L, 2L);
    }
}
