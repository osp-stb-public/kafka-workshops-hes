package de.osp.training.heskafka.asynctx.erp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import de.osp.training.heskafka.asynctx.erp.model.Address;
import de.osp.training.heskafka.asynctx.erp.model.CustomerOrder;
import de.osp.training.heskafka.asynctx.erp.repositories.CustomerOrderRepository;

@Component
public class DataLoader {
    @Autowired
    private CustomerOrderRepository customerOrderRepository;

    @EventListener(ApplicationReadyEvent.class)
    public void run() throws Exception {
        customerOrderRepository.save(new CustomerOrder(new Address("Max Mustermann", "Lützowplatz 73", "54429", "Heddert"), List.of()));
        customerOrderRepository.save(new CustomerOrder(new Address("Erika Mustermann", "Hallesches Ufer 96", "66663", "Merzig"), List.of()));
        customerOrderRepository.save(new CustomerOrder(new Address("John Doe", "Schaarsteinweg 99", "02892", "Reichenbach"), List.of()));
        customerOrderRepository.save(new CustomerOrder(new Address("Jane Doe", "Albrechtstrasse 52", "94012", "Passau"), List.of()));
        customerOrderRepository.save(new CustomerOrder(new Address("Otto Normalverbraucher", "Koenigstrasse 75", "07765", "Kahla"), List.of()));
        customerOrderRepository.save(new CustomerOrder(new Address("Hinz und Kunz", "Rathausstrasse 40", "90715", "Fürth"), List.of()));
    }
}
