package de.osp.training.heskafka.asynctx.erp.services;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import de.osp.training.heskafka.asynctx.erp.dto.AddressChangeInput;
import de.osp.training.heskafka.asynctx.erp.model.AddressChange;

@Service
public class AddressChangeServiceImpl implements AddressChangeService {
    private static final Logger logger = LoggerFactory.getLogger(AddressChangeServiceImpl.class);

    @Override
    public AddressChange addAddressChange(AddressChangeInput addressChangeInput, Long orderId) {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'addAddressChange'");
    }

    @Override
    public List<AddressChange> getAddressChanges(Long orderId) {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'getAddressChanges'");
    }

    @Override
    public AddressChange getAddressChange(Long orderId, Long addressChangeId) {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'getAddressChange'");
    }
    
}
