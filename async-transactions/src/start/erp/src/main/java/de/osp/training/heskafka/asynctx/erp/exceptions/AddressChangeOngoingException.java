package de.osp.training.heskafka.asynctx.erp.exceptions;

public class AddressChangeOngoingException extends RuntimeException {
    
    public AddressChangeOngoingException(Long orderId, Long addressChangeId) {
        super("Address change for order " + orderId + " is currently ongoing with id " + addressChangeId);
    }

}
