package de.osp.training.heskafka.asynctx.erp.services;

import java.util.List;

public interface DepotService {
    public List<Long> getAllDepots();
}
