package de.osp.training.heskafka.asynctx.submodule;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import de.osp.training.heskafka.asynctx.submodule.model.CustomerOrder;
import de.osp.training.heskafka.asynctx.submodule.repositories.CustomerOrderRepository;

@Component
public class DataLoader {
    @Autowired
    private CustomerOrderRepository customerOrderRepository;

    @EventListener(ApplicationReadyEvent.class)
    public void run() throws Exception {
        customerOrderRepository.save(new CustomerOrder(List.of()));
        customerOrderRepository.save(new CustomerOrder(List.of()));
        customerOrderRepository.save(new CustomerOrder(List.of()));
        customerOrderRepository.save(new CustomerOrder(List.of()));
        customerOrderRepository.save(new CustomerOrder(List.of()));
        customerOrderRepository.save(new CustomerOrder(List.of()));
    }
}
