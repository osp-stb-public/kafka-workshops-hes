package de.osp.training.heskafka.asynctx.submodule.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.osp.training.heskafka.asynctx.submodule.exceptions.CustomerOrderNotFoundException;
import de.osp.training.heskafka.asynctx.submodule.model.CustomerOrder;
import de.osp.training.heskafka.asynctx.submodule.repositories.CustomerOrderRepository;

@Service
public class CustomerOrderServiceImpl implements CustomerOrderService {
    @Autowired
    private CustomerOrderRepository customerOrderRepository;

    @Override
    public CustomerOrder getOrder(Long orderId) {
        return customerOrderRepository.findById(orderId).orElseThrow(() -> new CustomerOrderNotFoundException(orderId));
    }

}
