package de.osp.training.heskafka.asynctx.submodule;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class SubmoduleApplication {

	public static void main(String[] args) {
		SpringApplication.run(SubmoduleApplication.class, args);
	}

}
