package de.osp.training.heskafka.asynctx.submodule.model;

import java.time.Instant;

import com.fasterxml.jackson.annotation.JsonBackReference;

import jakarta.persistence.Embedded;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;

@Entity
public class AddressChange {
    @Id
    private Long id;
    @Embedded
    private Address address;
    @Enumerated(EnumType.STRING)
    private State state;
    private Instant lastStateChange;
    @ManyToOne
    @JoinColumn(name = "customerorder_id", nullable = false)
    @JsonBackReference
    private CustomerOrder order;

    public AddressChange() {
    }

    public AddressChange(Long id, Address address) {
        this.id = id;
        this.address = address;
        setState(State.WARTEN_AUF_BESTÄTIGUNG);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        setLastStateChange(Instant.now());
        this.state = state;
    }

    public Instant getLastStateChange() {
        return lastStateChange;
    }

    public void setLastStateChange(Instant lastStateChange) {
        this.lastStateChange = lastStateChange;
    }

    public CustomerOrder getOrder() {
        return order;
    }

    public void setOrder(CustomerOrder order) {
        this.order = order;
    }

    public enum State {
        WARTEN_AUF_BESTÄTIGUNG,
        ABGEBROCHEN,
        DURCHGEFÜHRT
    }

    @Override
    public String toString() {
        return "AddressChange [id=" + id + ", address=" + address + ", state=" + state + ", lastStateChange="
                + lastStateChange + "]";
    }

}
