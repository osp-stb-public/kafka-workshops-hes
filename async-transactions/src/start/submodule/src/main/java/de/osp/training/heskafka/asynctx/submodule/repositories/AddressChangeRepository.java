package de.osp.training.heskafka.asynctx.submodule.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import de.osp.training.heskafka.asynctx.submodule.model.AddressChange;

public interface AddressChangeRepository extends JpaRepository<AddressChange, Long> {
}
