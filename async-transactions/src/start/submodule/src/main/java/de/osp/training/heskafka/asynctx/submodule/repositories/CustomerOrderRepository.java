package de.osp.training.heskafka.asynctx.submodule.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import de.osp.training.heskafka.asynctx.submodule.model.CustomerOrder;

public interface CustomerOrderRepository extends JpaRepository<CustomerOrder, Long> {
}
