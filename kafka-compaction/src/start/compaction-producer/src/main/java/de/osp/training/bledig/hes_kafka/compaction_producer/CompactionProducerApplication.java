package de.osp.training.bledig.hes_kafka.compaction_producer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CompactionProducerApplication {

	public static void main(String[] args) {
		SpringApplication.run(CompactionProducerApplication.class, args);
	}

}
