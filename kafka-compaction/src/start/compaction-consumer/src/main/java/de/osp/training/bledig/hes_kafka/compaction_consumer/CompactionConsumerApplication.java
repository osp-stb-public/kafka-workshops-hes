package de.osp.training.bledig.hes_kafka.compaction_consumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CompactionConsumerApplication {

	public static void main(String[] args) {
		SpringApplication.run(CompactionConsumerApplication.class, args);
	}

}
