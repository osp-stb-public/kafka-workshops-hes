package de.osp.training.bledig.hes_kafka.compaction_producer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.annotation.EnableKafka;
import io.github.cdimascio.dotenv.Dotenv;

import java.util.Arrays;

@SpringBootApplication
@EnableKafka
public class CompactionProducerApplication implements CommandLineRunner {

	private static final Logger logger = LoggerFactory.getLogger(CompactionProducerApplication.class);

	@Autowired
	private ProductDemoDatenProducer productDemoDatenProducer;

	@Value("${NumberOfBatches:1}")
	private int numberOfBatches = 1;

	public static void main(String[] args) {
		// load environment variables from .env file and set them as system properties
		Dotenv.configure().systemProperties().load();
		// start the Spring Boot application
		SpringApplication.run(CompactionProducerApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		logger.info("Running with arguments: {}", Arrays.toString(args));
		if(args.length > 0 && args[0].startsWith("--delete")) {
			productDemoDatenProducer.deleteTestProducts();
			return;
		}
		long startAll = System.currentTimeMillis();
		for (int i = 0; i < numberOfBatches; i++) {
			logger.info("\n-----------------------------\nSending test products batch {} of {}\n------------------------------\n", i + 1, numberOfBatches);
			long start = System.currentTimeMillis();
			productDemoDatenProducer.sendTestProducts();
			long end = System.currentTimeMillis();
			logger.info("\n-----------------------------\nSent test products batch {} in {}s\n------------------------------\n", i + 1, (end - start)/1000.0);
		}
		long endAll = System.currentTimeMillis();
		logger.info("\n-----------------------------\nSent all test products {} batches in {}s\n------------------------------\n", numberOfBatches, (endAll - startAll)/1000.0);
	}

//	@Bean
//	public CommandLineRunner generateTestData(ProductDemoDatenProducer producer) {
//		return args -> producer.sendTestProducts();
//	}
}
