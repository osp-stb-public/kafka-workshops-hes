package de.osp.training.bledig.hes_kafka.compaction_producer;

import com.github.javafaker.Faker;
import de.osp.training.bledig.hes_kafka.common.ProductEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.Random;
import java.util.UUID;

@Service
public class ProductDemoDatenProducer {
    private static final String TOPIC = "hes-training.products";
    private static final int NUM_PRODUCTS = 100;
    private static final Logger logger = LoggerFactory.getLogger(ProductDemoDatenProducer.class);

    private static final Faker faker = new Faker(Locale.ENGLISH);
    private static final DateTimeFormatter formatter = DateTimeFormatter.ISO_LOCAL_DATE_TIME;
    private static final Random random = new Random();


    @Autowired
    private KafkaTemplate<String, ProductEvent> kafkaTemplate;

    public void sendTestProducts() {
        logger.info("Sending {} test products", NUM_PRODUCTS);
        for (int i = 0; i < NUM_PRODUCTS; i++) {
            ProductEvent product = generateProduct(i+1);
            String key = product.getProductId();
            kafkaTemplate.send(TOPIC, key, product);
            logger.info("Sent product: {}", product);
        }
    }

    private ProductEvent generateProduct(int ix) {
        return new ProductEvent(
                UUID.randomUUID().toString(),
                formatter.format(LocalDateTime.now()),
                UUID.randomUUID().toString(),
                buildProductId(ix), // deterministic product ID
                "Product " + ix, // deterministic product name
                faker.lorem().sentence(),
                Math.round(random.nextDouble() * 10000.0)/100.0
        );
    }

    public void deleteTestProducts() {
        logger.info("Sending delete tombstones for {} test products", NUM_PRODUCTS/3);
        for (int i = 0; i < NUM_PRODUCTS/3; i++) {
            String key = buildProductId(i);
            kafkaTemplate.send(TOPIC, key, null);
            logger.info("Sent delete tombstone for productId: {}", key);
        }
    }

    private String buildProductId(int ix) {
        return "4365a43bc434a2-" + ix;
    }
}
