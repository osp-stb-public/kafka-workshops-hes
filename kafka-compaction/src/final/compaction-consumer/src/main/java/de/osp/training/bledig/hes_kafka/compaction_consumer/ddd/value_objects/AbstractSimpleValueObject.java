package de.osp.training.bledig.hes_kafka.compaction_consumer.ddd.value_objects;

/**
 * Basis-Klasse für alle ValueObjects mit nur einem Attribute.
 */
abstract public class AbstractSimpleValueObject<T> {

    private final T value;

    protected AbstractSimpleValueObject(T value) {
        if(value == null)
            throw new IllegalArgumentException("Null as value not allowed!");
        this.value = value;
    }

    public T getValue() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AbstractSimpleValueObject)) return false;

        AbstractSimpleValueObject<?> that = (AbstractSimpleValueObject<?>) o;

        return getValue().equals(that.getValue());
    }

    @Override
    public int hashCode() {
        return getValue().hashCode();
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName()+"{"+value+'}';
    }
}

