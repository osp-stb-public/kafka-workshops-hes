package de.osp.training.bledig.hes_kafka.compaction_consumer;

import de.osp.training.bledig.hes_kafka.common.ProductEvent;
import de.osp.training.bledig.hes_kafka.compaction_consumer.ddd.ProductRepository;
import de.osp.training.bledig.hes_kafka.compaction_consumer.ddd.entities.ProductEntity;
import de.osp.training.bledig.hes_kafka.compaction_consumer.ddd.value_objects.ProductId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class ProductConsumer {
    private final Logger log = LoggerFactory.getLogger(ProductConsumer.class);
    private long messageCount = 0;
    private Map<String, Long> productCounts = new HashMap<>();

    @Autowired
    private ProductRepository productRepository;

    @KafkaListener(topics = "hes-training.products")
    public void consume(
            @Payload(required = false) ProductEvent productEvent, // required = false allows for tombstones
            @Header(KafkaHeaders.RECEIVED_KEY) String key // key is used for product ID
    ) {
        messageCount++;
        if(productEvent == null) {
            log.info("Received tombstone for key: {}", key);
            productRepository.delete(new ProductId(key));
            productCounts.remove(key);
        } else {
            log.debug("Received product: {}", productEvent);
            String productId = productEvent.getProductId();
            productCounts.put(productId, productCounts.getOrDefault(productId, 0L) + 1);
            productRepository.save(new ProductEntity(productEvent)); // store the latest product for each ID
            // Für Demozwecke alle x Nachrichten die Produkt-ID Häufigkeiten ausgeben bzw bei einer bestimmten Produkt-ID
            if(messageCount % 500 == 0 || productEvent.getProductId()=="4365a43bc434a2-99")
                printProductCounts();
        }
    }

    private void printProductCounts() {
        log.info("Product ID occurrences:");
        //productCounts.forEach((key, value) -> log.info(key + ": " + value));
        productCounts.entrySet().stream()
                .sorted(Map.Entry.comparingByKey())
                .forEach(entry -> log.info(entry.getKey() + ": " + entry.getValue()));
        log.info("Total messages: " + messageCount);
    }
}
