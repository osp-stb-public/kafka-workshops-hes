package de.osp.training.bledig.hes_kafka.compaction_consumer.ddd;

import de.osp.training.bledig.hes_kafka.compaction_consumer.ddd.entities.ProductEntity;
import de.osp.training.bledig.hes_kafka.compaction_consumer.ddd.value_objects.ProductId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class ProductRepository {

    private static final Logger log = LoggerFactory.getLogger(ProductRepository.class);
    private static final Map<ProductId, ProductEntity> products = new HashMap<>();

    public void save(ProductEntity product) {
        log.info("Saving product: {}", product);
        products.put(product.getProductId(), product);
    }

    public ProductEntity delete(ProductId productId) {
        log.info("Deleting product with {}", productId);
        return products.remove(productId);
    }
}