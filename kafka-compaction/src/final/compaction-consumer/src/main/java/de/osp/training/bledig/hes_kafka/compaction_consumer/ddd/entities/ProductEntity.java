package de.osp.training.bledig.hes_kafka.compaction_consumer.ddd.entities;

import de.osp.training.bledig.hes_kafka.common.ProductEvent;
import de.osp.training.bledig.hes_kafka.compaction_consumer.ddd.value_objects.ProductId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductEntity {
    private String updateAt;
    private ProductId productId;
    private String name;
    private String description;
    private double price;

    public ProductEntity(ProductEvent productEvent) {
        this.updateAt = productEvent.getEventDate();
        this.productId = new ProductId(productEvent.getProductId());
        this.name = productEvent.getName();
        this.description = productEvent.getDescription();
        this.price = productEvent.getPrice();
    }
}
