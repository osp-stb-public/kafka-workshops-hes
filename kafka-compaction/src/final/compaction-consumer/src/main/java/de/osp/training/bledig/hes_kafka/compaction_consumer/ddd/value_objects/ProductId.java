package de.osp.training.bledig.hes_kafka.compaction_consumer.ddd.value_objects;

public class ProductId extends AbstractSimpleValueObject<String> {

    public static final int MIN_LENGTH = 5;
    public static final int MAX_LENGTH = 50;

    public ProductId(String value) {
        super(value);
        // Validate the value
        if(value == null || value.isEmpty())
            throw new IllegalArgumentException("Product ID must not be null or empty!");
        if(value.length() < MIN_LENGTH)
            throw new IllegalArgumentException("Product ID to short!");
        if(value.length() > MAX_LENGTH)
            throw new IllegalArgumentException("Product ID to long!");
    }
}
