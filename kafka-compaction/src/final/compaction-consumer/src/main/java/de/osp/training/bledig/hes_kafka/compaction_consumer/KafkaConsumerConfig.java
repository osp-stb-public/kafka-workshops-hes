package de.osp.training.bledig.hes_kafka.compaction_consumer;

import de.osp.training.bledig.hes_kafka.common.ProductEvent;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.support.serializer.JsonDeserializer;

import java.util.HashMap;

@Configuration
@EnableKafka
public class KafkaConsumerConfig {

    // read the Kafka consumer group id from the application.properties file
    @Value("${spring.kafka.consumer.groupid}")
    private String groupId;

    @Value("${spring.kafka.bootstrap-servers}")
    private String bootstrapServers;

    // read the Kafka API key and secret from environment variables
    @Value("${CLUSTER_API_KEY}")
    private String apiKey;

    @Value("${CLUSTER_API_SECRET}")
    private String apiSecret;


    @Bean
    public ConsumerFactory<String, ProductEvent> consumerFactory() {
        var config = new HashMap<String, Object>();
        config.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        config.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest"); // Start from beginning, default is latest

        // set the consumer group id, its mandatory for Kafka and should be unique on context of this application
        config.put(ConsumerConfig.GROUP_ID_CONFIG, groupId);

        // set the client id, should be uniq for every instance of the consumer
        config.put(ConsumerConfig.CLIENT_ID_CONFIG, "compaction-consumer-client-bl1");

        // set the correct properties for authentication on confluent cloud
        config.put("security.protocol", "SASL_SSL");
        config.put("sasl.mechanism", "PLAIN");
        config.put("sasl.jaas.config", "org.apache.kafka.common.security.plain.PlainLoginModule required username=\""+apiKey+"\" password=\""+apiSecret+"\";");

        return new DefaultKafkaConsumerFactory<>(config, new StringDeserializer(), new JsonDeserializer<>(ProductEvent.class));
    }

    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, ProductEvent> kafkaListenerContainerFactory(
            ConsumerFactory<String, ProductEvent> consumerFactory) {
        ConcurrentKafkaListenerContainerFactory<String, ProductEvent> factory = new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(consumerFactory);
        return factory;
    }
}