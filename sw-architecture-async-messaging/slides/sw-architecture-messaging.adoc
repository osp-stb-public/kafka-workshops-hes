= SW-Architektur: Die Vorteile von asynchroner Kommunikation zwischen Microservices
:author: Bernd Ledig, Dresden 2024
:customcss: css/osp-slides3.css
:revealjs_theme: white
:revealjs_slideNumber: true
:imagesdir: ./images
:data-uri:
:source-highlighter: highlightjs
:icons: font

[.text-center]
image::abstract-architecuture.webp[abstract-arch, 800]



[.columns]
== Abstrakt

[.column]
image::abstract-architecuture2.webp[]

[.column]

* Asynchrone Kommunikation (aka Messaging) als Pattern u.a. für Microservices hat sich häufig als **überlegenes** Pattern bzgl. Punkt-to-Punkt API-Calls erwiesen.
* Es gibt allerdings auch hier Herausforderungen.


=== Was ist asynchrone Kommunikation?

Asynchrone Kommunikation bedeutet, dass Systeme Nachrichten senden und empfangen können, ohne auf eine sofortige Antwort warten zu müssen. 

Dies steht im Gegensatz zur synchronen Kommunikation, bei der ein System auf eine Antwort warten muss, bevor es weiterarbeiten kann.


== Klassische Architektur mit synchroner Kommunikation

image::synchron2.webp[]


=== Synchrone Kommunikation

image::api-architecture.drawio.png[]

=== Vorteile von synchroner Kommunikation

* **Einfachheit**: Synchrone Kommunikation ist einfach zu implementieren.
* **Kontrolle**: Synchrone Kommunikation bietet mehr Kontrolle über den Nachrichtenfluss als asynchrone Kommunikation.
* **Transparenz**: Synchrone Kommunikation ist transparenter als asynchrone Kommunikation.

=== Herausforderungen der synchroner Kommunikation

* **Skalierbarkeit**: Synchroner Kommunikationssysteme sind schwer zu skalieren, da sie auf eine Antwort warten müssen.
* **Fehleranfälligkeit**: Synchroner Kommunikationssysteme sind anfälliger für Fehler, da sie auf eine Antwort warten müssen.
* **Performance**: Synchroner Kommunikationssysteme sind langsamer als asynchrone Kommunikationssysteme, da sie auf **alle** Antworten warten müssen.

=== Herausforderungen der synchronen Kommunikation

* **Kopplung**: Synchrone Systeme sind sehr stark gekoppelt.
* **Verfügbarkeit**: Es ist schwierig, synchrone Systeme hoch verfügbar zu machen! +
  Der Ausfall eines Services führt oft zum Ausfall aller abhängigen Services.


== Asynchrone Kommunikation aka Messaging

image::messaging2.webp[]

[.no-title]
=== Messaging-Architektur

image::messaging-architecture.drawio.png[]

=== Vorteile von asynchroner Kommunikation

.Entkopplung
* **Keine direkten Abhängigkeiten**: Dienste agieren unabhängig voneinander und sind nicht direkt voneinander abhängig.
* **Bessere Skalierbarkeit**: Durch die Entkopplung der Dienste können einzelne Komponenten unabhängig voneinander skaliert werden.
* **Erhöhte Flexibilität**: Änderungen an einem Dienst haben keine unmittelbaren Auswirkungen auf andere Dienste.

=== Vorteile von asynchroner Kommunikation

.Fehlerisolierung
* **Robustheit**: Bei einem Ausfall eines Dienstes können andere Dienste weiterhin Nachrichten senden, die später verarbeitet werden können.
* **Fehlerbehandlung**: Nachrichten können in Warteschlangen verbleiben, bis der empfangende Dienst wieder verfügbar ist.

=== Vorteile von asynchroner Kommunikation

.Performance und Latenz
* **Nicht-blockierende Operationen**: Dienste können sofort weiterarbeiten, nachdem sie eine Nachricht gesendet haben, anstatt auf eine Antwort zu warten.
* **Geringere Last auf Netzwerkressourcen**: Asynchrone Kommunikation kann Netzwerkverkehr reduzieren, da nicht ständig Anfragen und Antworten zwischen Diensten ausgetauscht werden müssen.
* **Optimierte Ressourcennutzung**: Dienste können ihre Ressourcen effizienter nutzen, da sie nicht blockiert werden, während sie auf Antworten warten.

=== Vorteile von asynchroner Kommunikation

.Performance und Latenz
* **Bessere Handhabung von Lastspitzen**: Lastspitzen können durch Nachrichtenschlangen abgefangen werden, die Nachrichten zwischenspeichern und dann gleichmäßig an die verarbeitenden Dienste weiterleiten.


=== Vorteile von asynchroner Kommunikation

.Flexibilität bei der Entwicklung
* **Einfachere Integration von Drittanbietern**: Asynchrone Nachrichten ermöglichen eine einfachere und robustere Integration von Drittanbieterdiensten.
* **Erweiterbarkeit**: Neue Dienste können hinzugefügt werden, die auf bestehende Nachrichten reagieren, ohne dass bestehende Systeme geändert werden müssen.

=== Vorteile von asynchroner Kommunikation

.Robustheit gegenüber Netzwerkausfällen
* **Verlässliche Kommunikation**: Asynchrone Systeme sind weniger anfällig für temporäre Netzwerkausfälle, da Nachrichten zwischengespeichert und später zugestellt werden können.
* **Wiederholungsmechanismen**: Nachrichten können bei einem Fehler erneut gesendet werden, ohne dass der Benutzer oder das System manuell eingreifen muss.


=== Vorteile von asynchroner Kommunikation

.Verbesserte Benutzererfahrung
* **Reaktionsfähigkeit**: Anwendungen können sofort auf Benutzereingaben reagieren und die Verarbeitung im Hintergrund durchführen, was die wahrgenommene Leistung verbessert.
* **Nahtlose Interaktionen**: Durch die Entkopplung können Benutzer Aktionen durchführen, ohne durch lange Wartezeiten unterbrochen zu werden.


=== Herausforderungen der Asynchronen Kommunikation

.Komplexität der Implementierung
* Nachrichtenverwaltung: Das Handling von Nachrichten und deren Zustellgarantie erfordert zusätzliche Infrastruktur, wie z.B. Message Broker.
* Transaktionsmanagement: Sicherzustellen, dass Nachrichten zuverlässig und in der richtigen Reihenfolge zugestellt werden, kann komplex sein.

=== Herausforderungen der Asynchronen Kommunikation

.Fehlersuche und Debugging
* Nachvollziehbarkeit: Da Nachrichten asynchron verarbeitet werden, ist es schwieriger, den genauen Ablauf und die Ursachen von Fehlern nachzuvollziehen.
* Überwachung und Logging: Es erfordert spezielle Tools und Strategien, um die Zustände und den Fluss der Nachrichten zu überwachen.

=== Herausforderungen der Asynchronen Kommunikation

.Datenkonsistenz
* Eventual Consistency: In asynchronen Systemen kann es zu Verzögerungen bei der Konsistenz der Daten kommen, was zu Herausforderungen bei der Datenintegrität führen kann.
* Kompensationslogik: Bei Fehlern müssen Kompensationsmechanismen implementiert werden, um den Zustand des Systems zu korrigieren.


== Best Practices für Asynchrone Kommunikation

=== Verwendung eines geeigneten Message Brokers

Je nach Anforderungen und Umgebung können verschiedene Message Broker eingesetzt werden, um die asynchrone Kommunikation zu ermöglichen.

_Beispiele_: Apache Kafka, RabbitMQ, Google Pub/Sub, Amazon SQS +
_Vorteile_: Sie bieten robuste Mechanismen für Nachrichtenwarteschlangen, Zustellung und Fehlerbehandlung.

[.columns]
[.column]
image::apache-kafka-logo.png[kafka, 300]

[.column]
Als quasi Standard für Microservices-Architekturen hat sich **Kafka** etabliert, da es eine hohe Skalierbarkeit und Zuverlässigkeit bietet.


=== Idempotente Nachrichtenverarbeitung

Definition:: Sicherstellen, dass mehrfach empfangene Nachrichten keine unerwünschten Nebenwirkungen haben.
Umsetzung:: Durch das Design von Diensten, die dieselbe Nachricht mehrmals ohne negative Auswirkungen verarbeiten können.
  Insbesondere durch die Vergabe von eindeutigen IDs für Nachrichten.

=== Transaktionsmanagement

Definition:: Sicherstellen, dass Nachrichten zuverlässig und in der richtigen Reihenfolge zugestellt werden.
Umsetzung:: Durch die Implementierung von Transaktionen und Kompensationsmechanismen, um die Konsistenz der Daten zu gewährleisten
  (siehe u.a. auch das 
  https://de.wikipedia.org/wiki/Saga_(Entwurfsmuster)[SAGA-Pattern]).

=== Dead Letter Queues

Definition:: Warteschlangen, in denen Nachrichten gespeichert werden, die nicht erfolgreich verarbeitet werden konnten.
Umsetzung:: Nachrichten werden in Dead Letter Queues verschoben, wenn sie nicht erfolgreich verarbeitet werden können.
  Dies ermöglicht es, fehlerhafte Nachrichten zu identifizieren, zu untersuchen und nachträglich wieder zu verarbeiten.

=== Monitoring und Observability

Tools:: _OnPremise_: Prometheus, Grafana, ELK Stack, ... +
  _SaaS_: Datadog, Splunk, AWS CloudWatch, Google Stackdriver, ...
Strategien:: Implementierung umfassender Logging- und Monitoring-Lösungen, 
  um den Zustand des Systems zu überwachen und Probleme schnell zu identifizieren. +
  Nutzung von Standard-Protokollen wie OpenTracing, OpenCensus, OpenTelemetry.


[.columns]
== Fazit

[.column]
image::messaging-vs-synchron.webp[]

[.column]
Asynchrone Kommunikation ist in vielen Szenarien, insbesondere in Microservice-Architekturen, **überlegen** gegenüber synchronen Punkt-zu-Punkt API-Calls. 

=== Fazit

Trotz der Herausforderungen, die mit der Implementierung einhergehen, können die richtigen Strategien und Tools dazu beitragen, ein robustes und skalierbares System zu entwickeln, welches deutlich einfacher zu erweitern oder anzupassen sein kann.

=== Fazit

Asynchrone Kommunikation bietet eine Vielzahl von Vorteilen, die auch über die bereits diskutierten hinausgehen. Diese Vorteile tragen dazu bei, Systeme robuster, skalierbarer und effizienter zu gestalten. Durch die richtige Planung und Implementierung können Unternehmen diese Vorteile nutzen, um moderne und zukunftssichere Architekturen zu entwickeln.

[%notitle]
=== Vielen Dank!

image::thank-you.webp[]

